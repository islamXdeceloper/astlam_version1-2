


import Foundation
import Alamofire
import SwiftyJSON
import StatusAlert


class APIs : NSObject  {
    
    
    static func GetClientOrderscompletion ( completion : @escaping (_ error:Error? , _ userInfo : [GetClientOrders]? )->Void ) {
        
    
        guard  helper.get_Phone_Number() != nil else {return}
        
        
    let url = URLs.GetClientOrders
    
    
    let parameters = [
        "clientPhone" : helper.get_Phone_Number()!
    ]
    
    let headers = [
        "" : ""
    ]
        
        
        Alamofire.request(url,
                          method: .get ,
                          parameters: parameters ,
                          encoding: URLEncoding.default ,
                          headers: headers).responseJSON { (response) in
 
            print("parameter = \(parameters) , ==========> url ==========> \(url)")

            guard let data = response.data else {return}
                            
            switch response.result {
                
                case .failure(let error):
                    
                 LoadingIndicator.shared.Hide()
                    if error.localizedDescription == "cancelled" {
                        
                                               //   Loader.hide()
                                              }else if error.localizedDescription == "The Internet connection appears to be offline." {
                        
                     Loader.ShowError("يبدو انك غير متصل بالانترنت")
                                                  // code = -1009
                                                  // To Stop Animating And hide loading Indicator
                                               //   Loader.showError(message: "The Internet Connection is offline")
                                                  //  }
                                              }else if error.localizedDescription == "A server with the specified hostname could not be found." {
                     Loader.ShowError("معذزه يوجد مشكله في السيرفر")
                              
                                                  //code = -1003
                                              //    Loader.showError(message: "A server with the specified hostname could not be found.")
                              
                                              }
                 completion(error,nil)

                
                
                
                
            case .success(let value):
                let json = JSON(value)
                
                print(json)

                    do {
                        
                        
                        let get =  try JSONDecoder().decode([GetClientOrders].self, from: data)
                        
                        completion(nil,get)
                    } catch let jsonError {
                        print(jsonError)
                    }
                    

                
            }
        }
    }
    
    
    
    
}




/*
 
 APIs.getData(method : .get,
     parameter: ["clientPhone" : ""],url: URLs.GetClientOrders)
     { (clientOrders: [GetClientOrders]?, error) in
           if let err = error {
               print(err)
             LoadingIndicator.shared.Hide()

           } else {
               guard let clientOrders = clientOrders else { return }
               self.clientOrders = clientOrders
             //  self.scrollView.isHidden = false
            //   self.tableView.reloadData()
            // LoadingIndicator.shared.Hide()
            //   DispatchQueue.main.async {
        //   }
       }
   }
 }

 **/






