//
//  UpdateClientToken.swift
//  Astlam
//
//  Created by Mostafa  on 6/17/20.
//  Copyright © 2020 Eslam Ahmed. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON



extension APIs {
    
    
    static func UpdateClientToken()  {
        
        
        
        let url = URLs.UpdateClientToken
        
        let parameters = [
            "UserPhone" : helper.get_Phone_Number() ,
            "Token" : ""
        ]
        
        
        let headers = [
            "Accept" : "application/json"
        ]
        
        Alamofire.request(url,
                          method: .post,
                          parameters: parameters,
                          encoding: JSONEncoding.prettyPrinted,
                          headers: headers)
            .responseJSON { (response) in
            
             switch response.result {
            
                        case .failure(let error):
                            
                         LoadingIndicator.shared.Hide()
                            if error.localizedDescription == "cancelled" {
                                
                                                       //   Loader.hide()
                                                      }else if error.localizedDescription == "The Internet connection appears to be offline." {
                                
                             Loader.ShowError("يبدو انك غير متصل بالانترنت")
                                                          // code = -1009
                                                          // To Stop Animating And hide loading Indicator
                                                       //   Loader.showError(message: "The Internet Connection is offline")
                                                          //  }
                                                      }else if error.localizedDescription == "A server with the specified hostname could not be found." {
                             Loader.ShowError("معذزه يوجد مشكله في السيرفر")
                                      
                                                          //code = -1003
                                                      //    Loader.showError(message: "A server with the specified hostname could not be found.")
                                      
                                                      }

                        //   completion(error, nil)
            
            
                            print("\(error)")
            
                        case .success(let value):
            
//                            let json = JSON(value)
            
//                            if json["success"].intValue == 1 {
//
//                                let dataArr = json["feed"]
//
////                                let userinfo = UserInfo_Class(json: dataArr)
////                                print(userinfo)
//
////                                completion(nil , userinfo)
////                                Loader.hide()
//                            }else {
//
////                                Loader.hide()
//                            }
            
                            print(value)
                        }
                                }
    }
    
}
