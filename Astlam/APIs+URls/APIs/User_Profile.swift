////
////  User_Profile.swift
////  TURBO
////
////  Created by a on 1/23/20.
////  Copyright © 2020 pc. All rights reserved.
////
//
//import Foundation
//import Alamofire
//import SwiftyJSON
//
//
//extension API {
//    
//    class func Get_User_Info( completion: @escaping (_ error:Error? , _ userInfo : UserInfo_Class? )->Void){
//        
//        
//        if helper.getApiToken() == nil {
//            return
//        }
//        
//        let url = URLs.GetProfileData
//        
//    
//        
//        let headers : HTTPHeaders = [
//            "Accept" : "application/json" ,
//            "Content-Type" : "application/json" ,
//            "Authorization" : "Bearer " + helper.getApiToken()!
//        ]
//        
//        
//        AF.request(url, method: .post, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseJSON { (response) in
//            
//            switch response.result {
//                
//            case .failure(let error):
//                if error.localizedDescription == "cancelled" {
//                    Loader.hide()
//                }else if error.localizedDescription == "The Internet connection appears to be offline." {
//                    // code = -1009
//                    // To Stop Animating And hide loading Indicator
//                    Loader.showError(message: "The Internet Connection is offline")
//                    //  }
//                }else if error.localizedDescription == "A server with the specified hostname could not be found." {
//                    
//                    //code = -1003
//                    Loader.showError(message: "A server with the specified hostname could not be found.")
//                    
//                }
//                
//               completion(error, nil)
//                
//                
//                print("\(error)")
//                
//            case .success(let value):
//                
//                let json = JSON(value)
//                
//                if json["success"].intValue == 1 {
//                    
//                    let dataArr = json["feed"]
//                    
//                    let userinfo = UserInfo_Class(json: dataArr)
//                    print(userinfo)
//                    
//                    completion(nil , userinfo)
//                    Loader.hide()
//                }else {
//                    
//                    Loader.hide()
//                }
//               
//                print(value)
//            }
//            
//        }
//        
//    }
//}
