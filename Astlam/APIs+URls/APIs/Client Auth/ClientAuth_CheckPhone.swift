//
//  ClientAuth_CheckPhone.swift
//  Astlam
//
//  Created by Mostafa  on 6/21/20.
//  Copyright © 2020 Eslam Ahmed. All rights reserved.
//

//import Foundation
//import Alamofire
//import SwiftyJSON
//import StatusAlert
//
//
//extension APIs {
//
//
//    class func ClientAuth_CheckPhone(_ phoneNumber :  String) {
//
//        let url = URLs.ClientAuth_CheckPhone
//
//
//        let paramters = [
//            "phoneNumber" : phoneNumber
//
//        ]
//
//
//        let headers = [
//            "" : ""
//        ]
//
//
//
//               Alamofire.request(url,
//                                 method: .get ,
//                                 parameters: paramters ,
//                                 encoding: URLEncoding.default ,
//                                 headers: headers).responseJSON { (response) in
//
//                   print("parameter = \(paramters) , ==========> url ==========> \(url)")
//
//                   switch response.result {
//
//                       case .failure(let error):
//
//                        LoadingIndicator.shared.Hide()
//                           if error.localizedDescription == "cancelled" {
//
//                                                      //   Loader.hide()
//                                                     }else if error.localizedDescription == "The Internet connection appears to be offline." {
//
//                            Loader.ShowError("يبدو انك غير متصل بالانترنت")
//                                                         // code = -1009
//                                                         // To Stop Animating And hide loading Indicator
//                                                      //   Loader.showError(message: "The Internet Connection is offline")
//                                                         //  }
//                                                     }else if error.localizedDescription == "A server with the specified hostname could not be found." {
//                            Loader.ShowError("معذزه يوجد مشكله في السيرفر")
//
//                                                         //code = -1003
//                                                     //    Loader.showError(message: "A server with the specified hostname could not be found.")
//
//                                                     }
//
////                           completion(error,nil)
//
//
//
//
//                   case .success(let value):
//
//                    print(value)
////                               completion(nil,get)
//
//
//
//                   }
//               }
//
//    }
//}


import Foundation
import Alamofire
import SwiftyJSON
import StatusAlert


extension APIs {
    
    
    class func ClientAuth_CheckPhone(_ phoneNumber :  String , completion : @escaping (_ success : Bool) -> Void ) {
        
        let url = URLs.ClientAuth_CheckPhone
        
        
        let paramters = [
            "phoneNumber" : phoneNumber
        
        ]
        
        
        let headers = [
            "" : ""
        ]
            

        
               Alamofire.request(url,
                                 method: .get ,
                                 parameters: paramters ,
                                 encoding: URLEncoding.default ,
                                 headers: headers).responseJSON { (response) in
        
                   print("parameter = \(paramters) , ==========> url ==========> \(url)")
                                   
                   switch response.result {
                       
                       case .failure(let error):
                           
                        LoadingIndicator.shared.Hide()
                           if error.localizedDescription == "cancelled" {
                               
                                                      //   Loader.hide()
                                                     }else if error.localizedDescription == "The Internet connection appears to be offline." {
                               
                            Loader.ShowError("يبدو انك غير متصل بالانترنت")
                                                         // code = -1009
                                                         // To Stop Animating And hide loading Indicator
                                                      //   Loader.showError(message: "The Internet Connection is offline")
                                                         //  }
                                                     }else if error.localizedDescription == "A server with the specified hostname could not be found." {
                            Loader.ShowError("معذزه يوجد مشكله في السيرفر")
                                     
                                                         //code = -1003
                                                     //    Loader.showError(message: "A server with the specified hostname could not be found.")
                                     
                                                     }
                            //  Code=-1001 "The request timed out."
                        
                           else if error.localizedDescription == "The request timed out." {
                            Loader.ShowError("The request timed out.")
                        }

                        completion(false)

                       
                       
                       
                   case .success(let value):
                               
                    print(value)
                    
                    
                    
                    
                               completion(true)
                           

                       
                   }
               }

    }
}
