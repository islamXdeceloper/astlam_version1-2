//
//  ClientAuth_GetProfile.swift
//  Astlam
//
//  Created by Mostafa  on 6/21/20.
//  Copyright © 2020 Eslam Ahmed. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import StatusAlert


extension APIs {
    
    
    class func ClientAuth_GetProfile( completion : @escaping (_ error:Error? , _ userInfo : ClientAuth_Getprofile_Struct? )->Void) {
        
        
//        guard helper.get_Phone_Number() == nil else {
//            return
//        }
        
        let url = URLs.GetProfile
        
        
        let paramters = [
            "phoneNumber" : helper.get_Phone_Number()!
        ]
        
        
        let headers = [
            "" : ""
        ]
            

        
               Alamofire.request(url,
                                 method: .get ,
                                 parameters: paramters ,
                                 encoding: URLEncoding.default ,
                                 headers: headers).responseJSON { (response) in
        
                   print("parameter = \(paramters) , ==========> url ==========> \(url)")

                   guard let data = response.data else {return}

                   switch response.result {
                       
                       case .failure(let error):
                           
                        LoadingIndicator.shared.Hide()
                           if error.localizedDescription == "cancelled" {
                               
                                                      //   Loader.hide()
                                                     }else if error.localizedDescription == "The Internet connection appears to be offline." {
                               
                            Loader.ShowError("يبدو انك غير متصل بالانترنت")
                                                         // code = -1009
                                                         // To Stop Animating And hide loading Indicator
                                                      //   Loader.showError(message: "The Internet Connection is offline")
                                                         //  }
                                                     }else if error.localizedDescription == "A server with the specified hostname could not be found." {
                            Loader.ShowError("معذزه يوجد مشكله في السيرفر")
                                     
                                                         //code = -1003
                                                     //    Loader.showError(message: "A server with the specified hostname could not be found.")
                                     
                                                     }
                           completion(error,nil)

                       
                       
                       
                   case .success(let value):
                               
                    print(value)


    do {
        
        
        let get =  try JSONDecoder().decode(ClientAuth_Getprofile_Struct.self, from: data)
        
        completion(nil,get)
    } catch let jsonError {
        print(jsonError)
    }
    

                           

                       
                   }
               }

    }
}
