//
//  :api:ClientAuth_CheckClientNumberPhone.swift
//  Astlam
//
//  Created by Mostafa  on 6/21/20.
//  Copyright © 2020 Eslam Ahmed. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON


extension APIs {
    
    
    class func CAuth_CheckClientNumberPhone(_ phoneNumber :  String , _ code : String , completion : @escaping (_ error:Error? , _ message : String? )->Void) {
        
        let url = URLs.ClientAuth_CheckClientNumberPhone
        
        
        let paramters = [
            "PhoneNumber" : phoneNumber ,
            "Code" : code
        ]
        
        
        let headers = [
            "" : ""
        ]
            

        
               Alamofire.request(url,
                                 method: .post ,
                                 parameters: paramters ,
                                 encoding: JSONEncoding.prettyPrinted,
                                 headers: headers).responseJSON { (response) in
        
                   print("parameter = \(paramters) , ==========> url ==========> \(url)")
                                   
                   switch response.result {
                       
                       case .failure(let error):
                           
                        LoadingIndicator.shared.Hide()
                           if error.localizedDescription == "cancelled" {
                               
                                                      //   Loader.hide()
                                                     }else if error.localizedDescription == "The Internet connection appears to be offline." {
                               
                            Loader.ShowError("يبدو انك غير متصل بالانترنت")
                                                         // code = -1009
                                                         // To Stop Animating And hide loading Indicator
                                                      //   Loader.showError(message: "The Internet Connection is offline")
                                                         //  }
                                                     }else if error.localizedDescription == "A server with the specified hostname could not be found." {
                            Loader.ShowError("معذزه يوجد مشكله في السيرفر")
                                     
                                                         //code = -1003
                                                     //    Loader.showError(message: "A server with the specified hostname could not be found.")
                                     
                                                     }
//                           completion(error,nil)

                       
                       
                       
                   case .success(let value):
                          
                    let json = JSON(value)
                    
                    if let message = json["message"].string {
                        completion(nil , message)
                        
                    }
//                               completion(nil,get)
                     print(json)

                       
                   }
               }

    }
}
