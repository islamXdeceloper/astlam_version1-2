//
//  General_App.swift
//  Astlam
//
//  Created by Mostafa  on 6/22/20.
//  Copyright © 2020 Eslam Ahmed. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

extension APIs {
    
    
    
    static func Get_GeneralApp ( completion : @escaping (_ error:Error? , _ userInfo : GeneralApp_Struct? )->Void ) {
        
    
    let url = URLs.App
    
    
    let parameters = [
        "" : ""
    ]
    
    let headers = [
        "" : ""
    ]
        
        
        Alamofire.request(url,
                          method: .get ,
                          parameters: parameters ,
                          encoding: URLEncoding.default ,
                          headers: headers).responseJSON { (response) in
 
            print("parameter = \(parameters) , ==========> url ==========> \(url)")

            guard let data = response.data else {return}
                            
            switch response.result {
                
                case .failure(let error):
                    
                 LoadingIndicator.shared.Hide()
                    if error.localizedDescription == "cancelled" {
                        
                                               //   Loader.hide()
                                              }else if error.localizedDescription == "The Internet connection appears to be offline." {
                        
                     Loader.ShowError("يبدو انك غير متصل بالانترنت")
                                                  // code = -1009
                                                  // To Stop Animating And hide loading Indicator
                                               //   Loader.showError(message: "The Internet Connection is offline")
                                                  //  }
                                              }else if error.localizedDescription == "A server with the specified hostname could not be found." {
                     Loader.ShowError("معذزه يوجد مشكله في السيرفر")
                              
                                                  //code = -1003
                                              //    Loader.showError(message: "A server with the specified hostname could not be found.")
                              
                                              }
                 completion(error,nil)

                
                
                
                
            case .success(let value):
                let json = JSON(value)
                
                print(json)

                    do {
                        
                        
                        let get =  try JSONDecoder().decode(GeneralApp_Struct.self, from: data)
                        
                        completion(nil,get)
                        
                    } catch let jsonError {
                        print(jsonError)
                    }
                    

                
            }
        }
    }
    
    
    
    
}
