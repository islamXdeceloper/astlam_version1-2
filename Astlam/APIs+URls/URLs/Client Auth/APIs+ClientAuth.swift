//
//  APIs+ClientAuth.swift
//  Astlam
//
//  Created by Mostafa  on 6/17/20.
//  Copyright © 2020 Eslam Ahmed. All rights reserved.
//

import Foundation


import Foundation



extension  URLs {
    
    
    ///Post
    static let UploadClientImage = mainURL + "api/ClientAuth/UploadClientImage"
    
    
    ///Post
    static let ClientAuth_CheckClientNumberPhone = mainURL + "api/ClientAuth/CheckClientNumberPhone"
    
    
    
    ///Get
    /// Paramter {  phoneNumber : String }
    static let ClientAuth_CheckPhone = mainURL + "api/ClientAuth/CheckPhone"
    
    
    
    
    ///Post
    static let AddClient = mainURL + "api/ClientAuth/AddClient"
    
    
    
    ///Post
    static let UpdateClient = mainURL + "api/ClientAuth/UpdateClient"
    
    
    
    ///Get
    static let GetProfile = mainURL + "api/ClientAuth/GetProfile"
    
    
    
    
    ///Get
    static let n = mainURL + "api/ClientAuth/n"
    
}




