//
//  APIs+Provider.swift
//  Astlam
//
//  Created by Mostafa  on 6/18/20.
//  Copyright © 2020 Eslam Ahmed. All rights reserved.
//

import Foundation


extension  URLs {
    
    ///POST
    /// BODY : - { "ProviderPhone": "string",     "Lat": 0,        "Log": 0
    
    static let UpdateLocation = mainURL + "api/Provider/UpdateLocation"
    
    
    ///GET
    ///Paramters : { providerPhone : String  }
    static let SetActive = mainURL + "api/Provider/SetActive"
    
    
    ///GET
    ///Paramters : { providerPhone : String  }
    static let SetNotActive = mainURL + "api/Provider/SetNotActive"
    
    
    
    ///GET
    ///Paramters : { providerPhone : String  }
    static let ProviderOrders = mainURL + "api/Provider/ProviderOrders"
    
    
    
    ///POST
    ///BODY :- {     "UserPhone": "string",   "Token": "string"   }
    static let UpdateProviderToken = mainURL + "api/Provider/UpdateProviderToken"
    
    
    
    ///POST
    static let ValuationProvider = mainURL + "api/Provider/ValuationProvider"
    

    
    ///GET
    ///Paramters : { providerPhone : String  }
    static let ProviderAccountAmount = mainURL + "api/Provider/ProviderAccountAmount"
    
    
    
}
