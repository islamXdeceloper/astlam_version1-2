//
//  URLs.swift
//  eCommerce
//
//  Created by Mostafa on 12/6/16.
//  Copyright © 2020 pc. All rights reserved.
//

struct URLs {
    
    /// Main Url
     static let mainURL = "http://astalem.com/"
    

    
    
    
    static let base_image_url = "http://astalem.com/Upload/"

    
    
    /// Get
    /// { Url :-  clientPhone  : string }
    static let GetClientOrders = mainURL + "api/Client/GetClientOrders"
    
    
    ///Get
    /// {Url :- clientPhone : string}
    static let ClientAccountAmount = mainURL + "api/Client/ClientAccountAmount"
    
    ///Get
    ///{URl cobonCode : string}
    static let ClientGetCoboneValue = mainURL + "api/Client/GetCoboneValue"
    
    
    
    ///Post
    /// {Body UserPhone : string , Token : string}
    static let UpdateClientToken = mainURL + "pi/Client/UpdateClientToken"
    
    // /api/Client/UpdateClientToken
}
