//
//  APIs+Order.swift
//  Astlam
//
//  Created by Mostafa  on 6/17/20.
//  Copyright © 2020 Eslam Ahmed. All rights reserved.
//

import Foundation

extension  URLs {
    
   
    ///POST
    static let AddOrder = mainURL + "api/Order/AddOrder"
    
    
    ///GET
    /// Paramters :- {  currentLat : String , currentLong : String ,  otherLat :  String  , otherLong : String}
    static let GetDistance = mainURL + "api/Order/GetDistance"
    
    
    
    
    ///GET
    ///Paramter : - {  clientLat : String ,  clientLong : String}
    static let GetNearProvider = mainURL + "api/Order/GetNearProvider"
    
    
    //GET
    ///Paramter :- { clientPhone : String ,  orderId : Int ,  providers : Array[Providers]}
    static let AddOrderInOrderState = mainURL + "api/Order/AddOrderInOrderState"
    
    
    ///GET
    ///Paramter:- {orderId : Int ,  providers : Array[Providers] }
    static let SendOrderToProvider = mainURL + "api/Order/SendOrderToProvider"
    
    
    ///GET
    ///Paramters : - {providers : Array[Providers]}
    static let ProviderBusy = mainURL + "api/Order/ProviderBusy"
    
    
    ///GET
    ///Paramters : -  { providerPhone : String ,  orderId : Int}
    static let TestSentOrder = mainURL + "api/Order/TestSentOrder"
    
    
    
    ///GET
    ///Paramters :-  { cobonCode : String }
    static let GetCoboneAmount = mainURL + "api/Order/GetCoboneAmount"
    
    
    ///GET
    ///Paramters : - {orderId : Int }
    static let ProviderGetOrderDetails = mainURL + "api/Order/ProviderGetOrderDetails"
    
    
    ///GET
    ///Parmaters:- {  providerPhone : String }
    static let ProviderRejectOrder = mainURL + "api/Order/ProviderRejectOrder"
    
    
    ///GET
    ///Paramters : -  {   orderId : Int   }
    static let ChangOrderToInRoad = mainURL + "api/Order/ChangOrderToInRoad"
    
    
    
    
    ///GET
    ///Paramters:- { orderId : Int  }
    static let ChangOrderToAttend = mainURL + "api/Order/ChangOrderToAttend"
    
    
    
    
    ///GET
    ///Paramters:- { orderId : Int  }
    static let ChangOrderEnded = mainURL + "api/Order/ChangOrderEnded"
    
    
    
    
    ///GET
    ///Paramters :- {  orderId : Int  }
    static let ProviderCancelOrder = mainURL + "api/Order/ProviderCancelOrder"
    
    
    
    ///GET
    ///Paramters :- {  orderId : Int  }
    static let ClientCancelOrder = mainURL + "api/Order/ClientCancelOrder"
    
    
    ///GET
    static let GetDistrict = mainURL + "api/Order/GetDistrict"
    
    
    
    ///GET
    ///Paramters :- {  orderId : Int  }
    static let GetOfferPrice = mainURL + "api/Order/GetOfferPrice"
    
    
    ///POST
    static let AddOfferPrice = mainURL + "api/Order/AddOfferPrice"
    
    
    
    ///POST
    static let ClientAcceptOfferPrice = mainURL + "api/Order/ClientAcceptOfferPrice"
    
    
    
    ///GET
    ///Paramters : -  { providerPhone : String ,  orderId : Int}
    static let GetTrackingOrderData = mainURL + "api/Order/GetTrackingOrderData"
    
    
    
    ///GET
    ///Paramters : -  { providerPhone : String ,  orderId : Int}
    static let GetDataDirectionToClient = mainURL + "api/Order/GetDataDirectionToClient"
    
    
}
