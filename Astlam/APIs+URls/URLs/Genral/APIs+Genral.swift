//
//  APIs+Genral.swift
//  Astlam
//
//  Created by Mostafa  on 6/17/20.
//  Copyright © 2020 Eslam Ahmed. All rights reserved.
//

import Foundation


extension  URLs {
    
    ///
    static let App = mainURL + "api/Genral/App"
    
    
    
    ///Get
    /// Paramters :- {clientPhone : string}
    static let TestReciveMessageFromFirebse = mainURL + "api/Genral/TestReciveMessageFromFirebse"
    
    
    ///Post
    static let AddSuggest = mainURL + "api/Genral/AddSuggest"
    
}
