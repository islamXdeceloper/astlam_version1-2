//
//  APIs+ProviderAuth.swift
//  Astlam
//
//  Created by Mostafa  on 6/18/20.
//  Copyright © 2020 Eslam Ahmed. All rights reserved.
//

import Foundation



extension  URLs {


    ///POST
    ///Paramters :- {   providerPhone : String ,    }
    static let UploadProviderImage = mainURL + "api/ProviderAuth/UploadProviderImage"
    
    
    
    ///POST
    ///Paramters :- {   ContentType : String , ContentDisposition : String  }
    static let UploadProviderProfileImage = mainURL + "api/ProviderAuth/UploadProviderProfileImage"
    

    
    ///POST
    static let CheckProviderNumberPhone = mainURL + "api/ProviderAuth/CheckProviderNumberPhone"
    
    
    
    ///GET
    ///Paramters :- {   phoneNumber : String   }
    static let ProviderAuthCheckPhone = mainURL + "api/ProviderAuth/CheckPhone"
    
    
    ///POST
    static let AddProvider = mainURL + "api/ProviderAuth/AddProvider"
    
    
    
    ///POST
    static let UpdateProvider = mainURL + "api/ProviderAuth/UpdateProvider"
    
    
    
    ///GET
    ///Paramters :- {   phoneNumber : String   }
    static let ProviderAuthGetProfile = mainURL + "api/ProviderAuth/GetProfile"
    
    
    
    ///GET
    static let ProviderAuth_n = mainURL + "api/ProviderAuth/n"
}
