//
//  enterDataMandopVC.swift
//  Astlam
//
//  Created by Eslam Ahmed on 6/21/20.
//  Copyright © 2020 Eslam Ahmed. All rights reserved.
//

import UIKit
import StatusAlert

class EnterDataMandopVC: UIViewController {

    @IBOutlet weak var carImage: UIImageView!
    @IBOutlet weak var baordImage: UIImageView!
    
    @IBOutlet weak var passportImage: UIImageView!
    
    @IBOutlet weak var licenceImage: UIImageView!
    
    @IBOutlet weak var applicationImage: UIImageView!
    
    @IBOutlet weak var licenceOptionalImage: UIImageView!
    
    @IBOutlet weak var forntCarImage: UIImageView!
    @IBOutlet weak var forwardCarImage: UIImageView!
    
    
    @IBOutlet weak var checjMark: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    var status = StatusAlert()
    var imagePicker = UIImagePickerController()
    
    @IBAction func registerTapped(_ sender: Any) {

      
//        guard UIImage(named: "\(carImage.image))") != nil else {
//            status.image = UIImage(named: "error")
//            status.title = "please enter image car"
//            status.showInKeyWindow()
//
//          return
//        }
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "CMandopLocationVC") 
                  vc.modalPresentationStyle = .fullScreen
                  vc.modalTransitionStyle = .crossDissolve
                  show(vc, sender: self)
        
    }
    
    @IBAction func checkTapped(_ sender: Any) {
        checjMark.isSelected = !checjMark.isSelected
                  if checjMark.isSelected
                  {
                      checjMark.setImage(UIImage(named: "checked_checkbox"), for: .normal)
                  }
                  else
                  {
                          checjMark.setImage(UIImage(named: "Rectangle"), for: .normal)
                  }
    }
    
    
    @IBAction func conditionTapped(_ sender: Any) {
    }
    
   
    @IBAction func forntCarTapped(_ sender: Any) {
    }
    
    @IBAction func forwardCarTapped(_ sender: Any) {
    }
    
    @IBAction func licenceOptionalTapped(_ sender: Any) {
    }
    @IBAction func applicationTapped(_ sender: Any) {
    }
    
    @IBAction func passportTapped(_ sender: Any) {
    }
    
    
    @IBAction func licenceTapped(_ sender: Any) {
    }
    
    @IBAction func boardNumberTapped(_ sender: Any) {
    }
    
    @IBAction func typeCarTapped(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
                        imagePicker.delegate = self
                        imagePicker.sourceType = .photoLibrary
                        imagePicker.allowsEditing = false
                        present(imagePicker, animated: true, completion: nil)
                    }
    }
    
    
    
    
}

extension EnterDataMandopVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
          picker.dismiss(animated: true, completion: nil)
          if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
              carImage.image = image
              carImage.contentMode = .scaleToFill
          }
        if let imgUrl = info[UIImagePickerController.InfoKey.imageURL] as? URL{
              let imgName = imgUrl.lastPathComponent
              let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
              let localPath = documentDirectory?.appending(imgName)

            let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
            let data = image.pngData()! as NSData
              data.write(toFile: localPath!, atomically: true)
              //let imageData = NSData(contentsOfFile: localPath!)!
              let photoURL = URL.init(fileURLWithPath: localPath!)//NSURL(fileURLWithPath: localPath!)
         
          }

      }
   
}
