//
//  ChooseAsMandopVC.swift
//  Astlam
//
//  Created by Eslam Ahmed on 6/18/20.
//  Copyright © 2020 Eslam Ahmed. All rights reserved.
//

import UIKit

class ChooseAsMandopVC: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
    @IBAction func tappedClient(_ sender: Any) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "ChooseLoginVC")
        vc.modalPresentationStyle = .fullScreen
        vc.modalTransitionStyle = .crossDissolve
        show(vc, sender: self)
        
    }
    @IBAction func tappedMandop(_ sender: Any) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "LoginAsMandopVC")
        vc.modalPresentationStyle = .fullScreen
        vc.modalTransitionStyle = .crossDissolve
        show(vc, sender: self)
        
        
    }
    
}
