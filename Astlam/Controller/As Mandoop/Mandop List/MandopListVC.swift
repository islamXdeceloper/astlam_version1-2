//
//  MandopListVC.swift
//  Astlam
//
//  Created by Eslam Ahmed on 6/24/20.
//  Copyright © 2020 Eslam Ahmed. All rights reserved.
//

import UIKit

class MandopListVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func sendTapped(_ sender: Any) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "MandopListChattVC")
               vc.modalPresentationStyle = .fullScreen
               vc.modalTransitionStyle = .crossDissolve
               show(vc, sender: self)
    }
    
    @IBAction func dismiss(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    
    

}
