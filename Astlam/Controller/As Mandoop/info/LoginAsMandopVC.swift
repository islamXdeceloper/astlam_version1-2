//
//  LoginAsMandopVC.swift
//  Astlam
//
//  Created by Eslam Ahmed on 6/20/20.
//  Copyright © 2020 Eslam Ahmed. All rights reserved.
//

import UIKit

class LoginAsMandopVC: UIViewController {
    
    
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var distention: UITextField!
    
    @IBOutlet weak var buttonPic: UIButtonX!
    var imagePicker = UIImagePickerController()
    
    @IBOutlet weak var imagePicture: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
   
    
    @IBAction func condtionTapped(_ sender: Any) {
    }
    @IBAction func back(_ sender: Any) {
      self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func continueTapped(_ sender: Any) {
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "enterDataMandopVC")
         vc.modalPresentationStyle = .fullScreen
         vc.modalTransitionStyle = .crossDissolve
         show(vc, sender: self)
    }
    
    @IBAction func pictureChoose(_ sender: Any) {
             if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
                   imagePicker.delegate = self
                   imagePicker.sourceType = .photoLibrary
                   imagePicker.allowsEditing = false
                   present(imagePicker, animated: true, completion: nil)
               }
       }
    
    
    

}


extension LoginAsMandopVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
          picker.dismiss(animated: true, completion: nil)
          if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
              imagePicture.image = image
              imagePicture.contentMode = .scaleToFill
          }
        if let imgUrl = info[UIImagePickerController.InfoKey.imageURL] as? URL{
              let imgName = imgUrl.lastPathComponent
              let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).first
              let localPath = documentDirectory?.appending(imgName)

            let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
            let data = image.pngData()! as NSData
              data.write(toFile: localPath!, atomically: true)
              //let imageData = NSData(contentsOfFile: localPath!)!
              let photoURL = URL.init(fileURLWithPath: localPath!)//NSURL(fileURLWithPath: localPath!)
              print(photoURL)
          }

      }
   
}
