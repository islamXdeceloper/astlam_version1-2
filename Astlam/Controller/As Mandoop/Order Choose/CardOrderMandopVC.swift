//
//  CardOrderMandopVC.swift
//  Astlam
//
//  Created by Eslam Ahmed on 6/21/20.
//  Copyright © 2020 Eslam Ahmed. All rights reserved.
//

import UIKit

class CardOrderMandopVC: UIViewController {

  @IBOutlet weak var handleArea: UIView!


  
    @IBAction func detailsBtn(_ sender: Any) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "MandopListVC")
          vc.modalPresentationStyle = .fullScreen
          vc.modalTransitionStyle = .crossDissolve
          show(vc, sender: self)
        
    }
    
}
