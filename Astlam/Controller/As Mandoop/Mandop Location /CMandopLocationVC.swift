//
//  CMandopLocationVC.swift
//  Astlam
//
//  Created by Eslam Ahmed on 6/21/20.
//  Copyright © 2020 Eslam Ahmed. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation


class CMandopLocationVC: BaseMandopVC,CLLocationManagerDelegate, MKMapViewDelegate {
    @IBOutlet weak var mapView: MKMapView!
    
    @IBOutlet weak var menu: UIBarButtonItem!
    
    @IBOutlet weak var notificationMandop: UIButton!
    
    @IBOutlet weak var refreshBtn: UIBarButtonItem!
    @IBOutlet weak var switchBtn: UISwitch!
    @IBOutlet weak var locationBtn: UIButton!
    let manager = CLLocationManager()
    var regionRadius: CLLocationDistance = 1000

    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
        centerMapOnUserLocation()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
                          
        self.navigationController?.navigationBar.shadowImage = UIImage()
//        addSlideMenuButton()

    }
    override func viewDidAppear(_ animated: Bool) {
          super.viewDidAppear(animated)
          manager.desiredAccuracy = kCLLocationAccuracyBest //battary
          manager.delegate = self
          manager.requestWhenInUseAuthorization()
          manager.startUpdatingLocation()
      }
    func getAddress(lat: Double, lng: Double, handler: @escaping  (String) -> Void) -> Void
       {
           var address: String = ""
           let geoCoder = CLGeocoder()
           let location = CLLocation(latitude: Double(lat), longitude: Double(lng))
           //selectedLat and selectedLon are double values set by the app in a previous process
           if #available(iOS 11.0, *) {
               geoCoder.reverseGeocodeLocation(location, preferredLocale: Locale.init(identifier: "ar_eg"), completionHandler: { (placemarks, error) -> Void in
                   // Place details
                   var placeMark: CLPlacemark?
                   placeMark = placemarks?[0]
                   // Location name
                   if let locationName = placeMark?.addressDictionary?["Name"] as? String {
                       address += locationName + ", "
                   }
                   // Street address
                   if let street =
                       placeMark?.addressDictionary?["Thoroughfare"] as? String {
                       address += street + ", "
                   }
                   // City
                   if let city = placeMark?.addressDictionary?["City"] as? String {
                       address += city + ", "

                   }
                   // Passing address back
                   handler(address)
               })
           } else {
           }
       }
       
       
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
         if let location = locations.first {
             manager.startUpdatingLocation()
             
             render(location)
         }
     }
      func render(_ location:CLLocation) {
          let coordinate = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
          let lng = self.manager.location!.coordinate.longitude
          let att = self.manager.location!.coordinate.latitude
            self.getAddress(lat: att, lng: lng) { (address) in
                      
              print(lng)
              print(att)
          }
    
          let span = MKCoordinateSpan(latitudeDelta: 0.1, longitudeDelta: 0.1)
          let region = MKCoordinateRegion(center: coordinate, span: span)
          mapView.setRegion(region, animated: true)
          
          let pin = MKPointAnnotation()
          pin.coordinate = coordinate

          mapView.addAnnotation(pin)

          
      }

    
    @IBAction func menuTapped(_ sender: Any) {
    }
    
    @IBAction func notificationTapped(_ sender: Any) {
    }
    
    @IBAction func refreshTapped(_ sender: Any) {
    }
    
    @IBAction func locationZoom(_ sender: Any) {
        centerMapOnUserLocation()
    }
    func centerMapOnUserLocation() {
        let coordinateRegion = MKCoordinateRegion(center: mapView.userLocation.coordinate, latitudinalMeters: regionRadius * 2.0, longitudinalMeters: regionRadius * 2.0)
        
        mapView.setRegion(coordinateRegion, animated: true)
    }

    
    @IBAction func switchTapped(_ sender: UISwitch) {
        if (sender.isOn == true){
              print("on")
          }
          else{
              print("off")
          }
    }
    


}
