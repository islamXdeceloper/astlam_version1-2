//
//  MenuMandopVC.swift
//  Astlam
//
//  Created by Eslam Ahmed on 6/24/20.
//  Copyright © 2020 Eslam Ahmed. All rights reserved.
//

import UIKit
protocol SlideMenuMadnopDelegate {
    func slideMenuItemSelectedAtIndex(_ index : Int32)
}
class MenuMandopVC: UIViewController,UITableViewDataSource, UITableViewDelegate {

  
        @IBOutlet weak var myCodeView: UIView!
        @IBOutlet weak var myCouponView: UIView!
        @IBOutlet weak var myOrderView: UIView!
        
        /**
        *  Array to display menu options
        */
        @IBOutlet var tblMenuOptions : UITableView!
        
        /**
        *  Transparent button to hide menu
        */
        @IBOutlet var btnCloseMenuOverlay : UIButton!
        
        /**
        *  Array containing menu options
        */
        var arrayMenuOptions = [Dictionary<String,String>]()
        /**
        *  Menu button which was tapped to display the menu
        */
        var btnMenu : UIButton?
        
        /**
        *  Delegate of the MenuVC
        */
        var delegate : SlideMenuMadnopDelegate?
        
        override func viewDidLoad() {
            super.viewDidLoad()
            tblMenuOptions.tableFooterView = UIView()
            // Do any additional setup after loading the view.
            tblMenuOptions.layer.cornerRadius = 15
                 view.bringSubviewToFront(tblMenuOptions)
                 tblMenuOptions.tableFooterView = UIView()
            updateArrayMenuOptions()

        }
        override func viewDidDisappear(_ animated: Bool) {
             super.viewDidDisappear(true)
             self.myCodeView.isHidden = true
             self.myOrderView.isHidden = true
             self.myCouponView.isHidden = true

         }
        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
            // Dispose of any resources that can be recreated.
        }
        

        
        @IBAction func ProfileTapped(_ sender: Any) {
             let sb = storyboard?.instantiateViewController(identifier: "ProfileVc")
            sb?.modalPresentationStyle = .fullScreen
            sb?.modalTransitionStyle = .crossDissolve
            self.present(sb!, animated: true, completion: nil)
        }
        func updateArrayMenuOptions(){
            arrayMenuOptions.append(["title":"طـلـبـاتــي", "icon":"client_icon","iconleft":"back_black"])
            arrayMenuOptions.append(["title":"حـسـابــي", "icon":"Account_mandop","iconleft":"back_black"])
            arrayMenuOptions.append(["title":"كود خصم", "icon":"code_mandop","iconleft":"back_black"])
            arrayMenuOptions.append(["title":"نبذة عنا", "icon":"about_mandop","iconleft":"back_black"])
            arrayMenuOptions.append(["title":"الدعم الفني", "icon":"support_mandop","iconleft":"back_black"])
            
            arrayMenuOptions.append(["title":"الإعدادات", "icon":"","iconleft":""])
            arrayMenuOptions.append(["title":"تسجيل خروج", "icon":"","iconleft":""])
            
         
            
              

            
            tblMenuOptions.reloadData()
        }
        
        @IBAction func onCloseMenuClick(_ button:UIButton!){
            btnMenu?.tag = 0
                 
                 if (self.delegate != nil) {
                     var index = Int32(button.tag)
                     if(button == self.btnCloseMenuOverlay){
                         index = -1
                     }
                     delegate?.slideMenuItemSelectedAtIndex(index)
                 }
            
            UIView.animate(withDuration: 0.3, animations: { () -> Void in
                self.view.frame = CGRect(x:  UIScreen.main.bounds.size.width, y: 0, width: UIScreen.main.bounds.size.width,height: UIScreen.main.bounds.size.height)
                self.view.layoutIfNeeded()
                self.view.backgroundColor = UIColor.clear
                }, completion: { (finished) -> Void in
                    self.view.removeFromSuperview()
                    self.removeFromParent()
            })
        }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

            
            let cell = tableView.dequeueReusableCell(withIdentifier: "MandopMenuCell", for: indexPath) as! MandopMenuCell

            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            cell.layoutMargins = UIEdgeInsets.zero
            cell.preservesSuperviewLayoutMargins = false
            cell.backgroundColor = UIColor.clear
            if indexPath.row == 6 {
                cell.labelFetched.textColor = .red
            }

            
            cell.imageHeader.image = UIImage(named: arrayMenuOptions[indexPath.row]["icon"]!)
            cell.labelFetched.text = arrayMenuOptions[indexPath.row]["title"]!
            cell.imageLeft.image =
                UIImage(named:arrayMenuOptions[indexPath.row]["iconleft"]!)
            
            
            return cell
        }
        
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            
            if indexPath.row == 0 {
             
                         self.myCouponView.isHidden = true
                         self.myOrderView.isHidden = false
                         self.myCodeView.isHidden = true

            }
            
            
            else if indexPath.row == 1 {
               
                self.myCouponView.isHidden = false
                self.myOrderView.isHidden = true
                self.myCodeView.isHidden = true


            }
            
            else if indexPath.row == 2 {
                
                self.myCouponView.isHidden = true
                self.myOrderView.isHidden = true
                self.myCodeView.isHidden = false


            } else if indexPath.row == 4 {
                let sb = storyboard?.instantiateViewController(identifier: "SupportVC")
                   sb?.modalPresentationStyle = .fullScreen
                   sb?.modalTransitionStyle = .crossDissolve
                show(sb!,sender: self)
               
            }
                 
        }
        
        

        
        
        
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return arrayMenuOptions.count
        }
        
        func numberOfSections(in tableView: UITableView) -> Int {
            return 1;
        }

    


}
