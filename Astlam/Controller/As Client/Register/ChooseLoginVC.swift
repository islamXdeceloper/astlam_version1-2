//
//  ChooseLoginVC.swift
//  Astlam
//
//  Created by Eslam Ahmed on 6/16/20.
//  Copyright © 2020 Eslam Ahmed. All rights reserved.
//

import UIKit

class ChooseLoginVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    @IBAction func ClientTapped(_ sender: Any) {
        
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "LoginControllerVC")
        vc.modalPresentationStyle = .fullScreen
        vc.modalTransitionStyle = .crossDissolve
        show(vc, sender: self)
        
        
        helper.save_Type(1)
        
        print("mostafa")
    }
    
    @IBAction func MandopTapped(_ sender: Any) {
           let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "ChooseAsMandopVC")
           vc.modalPresentationStyle = .fullScreen
           vc.modalTransitionStyle = .crossDissolve
           show(vc, sender: self)
        
        helper.save_Type(2)
    }
    
}
