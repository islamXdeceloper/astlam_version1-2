//
//  CurrentLocationVC.swift
//  Astlam
//
//  Created by Eslam Ahmed on 6/6/20.
//  Copyright © 2020 Eslam Ahmed. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import SideMenuController



class CurrentLocationVC: UIViewController, CLLocationManagerDelegate, MKMapViewDelegate , SideMenuControllerDelegate {
    
    
    func sideMenuControllerDidHide(_ sideMenuController: SideMenuController) {
        print(#function)
    }
    
    func sideMenuControllerDidReveal(_ sideMenuController: SideMenuController) {
        print(#function)
    }
    
    
    //MARK:-  <======= AutoLayout Constraints  =======>

    @IBOutlet weak var mapView: MKMapView!
    
    
    
    // MARK:- Variable & Constants
    var order_long_lat : Order_GetDistrict!
    
    let manager = CLLocationManager()
    

    var regionRadius: CLLocationDistance = 1000

    
    @IBOutlet weak var myLocation: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
     
        
        centerMapOnUserLocation()
        mapView.delegate = self
        sideMenuController?.delegate = self

        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
                     
        self.navigationController?.navigationBar.shadowImage = UIImage()
     

    }
 

    func centerMapOnUserLocation() {
           let coordinateRegion = MKCoordinateRegion(center: mapView.userLocation.coordinate, latitudinalMeters: regionRadius * 2.0, longitudinalMeters: regionRadius * 2.0)

           mapView.setRegion(coordinateRegion, animated: true)
       }
    
    
    func getAddress(lat: Double, lng: Double, handler: @escaping  (String) -> Void) -> Void
    {
        var address: String = ""
        let geoCoder = CLGeocoder()
        let location = CLLocation(latitude: Double(lat), longitude: Double(lng))
        //selectedLat and selectedLon are double values set by the app in a previous process
        if #available(iOS 11.0, *) {
            geoCoder.reverseGeocodeLocation(location, preferredLocale: Locale.init(identifier: "ar_eg"), completionHandler: { (placemarks, error) -> Void in
                // Place details
                var placeMark: CLPlacemark?
                placeMark = placemarks?[0]
                // Location name
                if let locationName = placeMark?.addressDictionary?["Name"] as? String {
                    address += locationName + ", "
                }
                // Street address
                if let street =
                    placeMark?.addressDictionary?["Thoroughfare"] as? String {
                    address += street + ", "
                }
                // City
                if let city = placeMark?.addressDictionary?["City"] as? String {
                    address += city + ", "
                }
                // Passing address back
                handler(address)
            })
        } else {
        }
    }

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        let sb = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "CustomSideMenuController")
//          window?.rootViewController = sb
        manager.desiredAccuracy = kCLLocationAccuracyBest //battary
        manager.delegate = self
        manager.requestWhenInUseAuthorization()
        manager.startUpdatingLocation()
    }
   
    
    
    
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            manager.startUpdatingLocation()
            
            render(location)
        }
    }
    
    
    
    
    
    
    func render(_ location:CLLocation) {
        
        let coordinate = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        let lng = self.manager.location!.coordinate.longitude
        let att = self.manager.location!.coordinate.latitude
            self.getAddress(lat: att, lng: lng) { (address) in
                    
            print(lng)
            print(att)
                self.myLocation.text = address
        }
  
        let span = MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01)
        let region = MKCoordinateRegion(center: coordinate, span: span)
        mapView.setRegion(region, animated: true)
        
              let pin = MKPointAnnotation()
                    pin.coordinate = coordinate
                    mapView.addAnnotation(pin)

        
    }
    
    
   
    
    
    
    
    
    @IBAction func SendOrder(_ sender: Any) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "customTwo")
               vc.modalPresentationStyle = .fullScreen
               vc.modalTransitionStyle = .crossDissolve
        present(vc, animated: true, completion: nil)

    }
    
    
    
    
    
    
//
//    @IBAction func NotificationRed(_ sender: Any) {
//        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "CLNotificationVC")
//                vc.modalPresentationStyle = .fullScreen
//                vc.modalTransitionStyle = .crossDissolve
//                show(vc, sender: self)
//
//    }
//
//
    
    
    
    
    
   
    
}


