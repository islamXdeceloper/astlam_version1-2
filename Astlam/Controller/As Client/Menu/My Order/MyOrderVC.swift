//
//  MyOrderVC.swift
//  Astlam
//
//  Created by Eslam Ahmed on 6/16/20.
//  Copyright © 2020 Eslam Ahmed. All rights reserved.
//








import UIKit

class MyOrderVC: UIViewController {

    @IBOutlet weak var tableView: UITableView!

    @IBOutlet weak var noOrderLB: UILabel!
    
    
    //MARK:- Variable & Constants
    
    var orderList = [GetClientOrders]()
    
//    lazy var refresher : UIRefreshControl = {
//        let refresher = UIRefreshControl()
//        refresher.addTarget(self, action: #selector(GetClientOrder), for: .valueChanged)
//        return refresher
//    }()
//
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
//        self.view.bounds.size.width = self.view.bounds.size.width / 2

        self.tableView.tableFooterView = UIView()
//        self.tableView.backgroundColor = .red
//        self.view.addSubview(refresher)
        GetClientOrder()
        
        
        
        

    }
    
   
    
    
    func GetClientOrder() {
        
    LoadingIndicator.shared.Show(indicator: self.view)
        
    
    APIs.GetClientOrderscompletion { (error, orders) in
        if let orders = orders {
            self.orderList = orders
            
                if self.orderList.count == 0 {
                    LoadingIndicator.shared.Hide()
                    self.noOrderLB.isHidden = false
                    self.tableView.isHidden = true
                    
            }else {
                    LoadingIndicator.shared.Hide()
                LoadingIndicator.shared.Hide()
                    self.noOrderLB.isHidden = true

                self.tableView.isHidden = false
                self.tableView.reloadData()
                
            }
        }
    }
    }
    

}



extension MyOrderVC : UITableViewDelegate , UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orderList.count
    }
   
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "myOrderCell", for: indexPath) as! myOrderCell
        
        
        cell.isHidden = false
        print(indexPath.row)
        
        
        
//        cell.orderNumber.text = "mostafa"
        
        return cell
    }
    
    
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return self.tableView.bounds.size.height / 3.4
    }
}
