//
//  MenuTableCell.swift
//  Astlam
//
//  Created by Eslam Ahmed on 7/1/20.
//  Copyright © 2020 Eslam Ahmed. All rights reserved.
//

import UIKit
class MenuTableCell: UITableViewCell {

  
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var img: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        label.textAlignment = NSTextAlignment.right
        
       // img.trailingAnchor.constraint(equalTo: margins.trailingAnchor, constant: -10).isActive = true
    
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
