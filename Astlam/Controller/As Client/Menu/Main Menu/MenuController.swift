//
//  MenuController.swift
//  Astlam
//
//  Created by Eslam Ahmed on 7/1/20.
//  Copyright © 2020 Eslam Ahmed. All rights reserved.
//

import UIKit
import Kingfisher



struct ModelTableViewCell {
    var HeaderName:String = ""
    var image:UIImage!

}

class MenuController: UITableViewController {
    var arrData = [ModelTableViewCell]()

//    let segue = [
//        "showCenterController1",
//        "showCenterController2"
//                ]

    private var previousIndex: NSIndexPath?
    //MARK:- Variable & Constants
    var data = [
        ModelTableViewCell(HeaderName: "", image: UIImage(named: "")),
        ModelTableViewCell(HeaderName: "الرئيسية", image: UIImage(named: "myOrder")),
        ModelTableViewCell(HeaderName: "طلباتي", image: UIImage(named: "myOrder")),
        ModelTableViewCell(HeaderName: "حسابي", image: UIImage(named: "myAccount")),
        ModelTableViewCell(HeaderName: "كود خصم", image: UIImage(named: "Code")),
        ModelTableViewCell(HeaderName: "نبذه عنا", image: UIImage(named: "about")),
        ModelTableViewCell(HeaderName: "الدعم الفني", image: UIImage(named: "support")),
        ModelTableViewCell(HeaderName: "الاعدادات", image: UIImage(named: "")),
        ModelTableViewCell(HeaderName: "تسجيل خروج", image: UIImage(named: ""))
             ]

var userData = ClientAuth_Getprofile_Struct()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        GetUserProfile()
   
        
    }
     fileprivate func GetUserProfile(){
            
//        LoadingIndicator.shared.Show(indicator: self.view)
        APIs.ClientAuth_GetProfile { (error, profile) in
            if let profile = profile {
                self.userData = profile
                UIView.animate(withDuration: 0.5) {
                    LoadingIndicator.shared.Hide()
                    self.tableView.reloadData()
                    self.view.layoutIfNeeded()
                }
   
            }else {
    //            self.view.isHidden = true

//                LoadingIndicator.shared.Hide()
                Loader.ShowError("حدث خطا ما")
            }
        }

        }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }


    override func tableView(_ tableView: UITableView,
                            cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 && indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "profileCell", for: indexPath) as! ProfileTableViewCell
            
            cell.phoneNumber.text = self.userData.phone
            cell.username.text = self.userData.fullName
            cell.imageProfile.kf.setImage(with: URL(string: URLs.base_image_url + self.userData.image! ))

            return cell
        }
        else   {
            let cell = tableView.dequeueReusableCell(withIdentifier: "menuCell", for: indexPath) as! MenuTableCell

            if indexPath.row >= 2 {
            cell.label.text = data[indexPath.row].HeaderName
            cell.img.image = data[indexPath.row].image
            }
            if indexPath.row > 6 {
                cell.label.text = data[indexPath.row].HeaderName
                cell.img.image = .none
                cell.textLabel?.textAlignment = .right
            }
            if indexPath.row == 8 {
                cell.label.text = data[indexPath.row].HeaderName
                cell.img.image = .none
                cell.textLabel?.textAlignment = .right
                cell.label.textColor = .red
            }
            return cell
        }
    }
    override func tableView(_ tableView: UITableView,
            didSelectRowAt indexPath: IndexPath)  {
        if let index = previousIndex {
            tableView.deselectRow(at: index as IndexPath, animated: true)
        }
    let sb = UIStoryboard(name: "Main", bundle: nil)
        // to go user details and edit
        if indexPath.row == 0 {
            let vc = sb.instantiateViewController(identifier: "ProfileVc") as ProfileVc
                vc.modalPresentationStyle = .fullScreen
                vc.modalTransitionStyle = .crossDissolve
                show(vc, sender: self)
        }
        if indexPath.row == 1 {
            
            let vc = sb.instantiateViewController(identifier: "CustomSideMenuController")
                vc.modalPresentationStyle = .fullScreen
                vc.modalTransitionStyle = .crossDissolve
                show(vc, sender: self)

//                    vc.modalPresentationStyle = .fullScreen
//                    vc.modalTransitionStyle = .crossDissolve
                    show(vc, sender: self)
        }
        if indexPath.row == 2 {

               let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "MyOrderVC")
           
//                          vc.modalPresentationStyle = .fullScreen
//                          vc.modalTransitionStyle = .crossDissolve
                          show(vc, sender: self)
              }
        if indexPath.row == 3 {
                   let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "MyCardVC")
//                              vc.modalPresentationStyle = .fullScreen
//                              vc.modalTransitionStyle = .crossDissolve
                              show(vc, sender: self)
                  }
        
        
        if indexPath.row == 4 {

            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "MyCodeVC")
            show(vc, sender: self)

        }
 
        }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if indexPath.section == 0 && indexPath.row == 0 {
            return 260
        } else  {
            return 60
        }
    
    }
  
}
