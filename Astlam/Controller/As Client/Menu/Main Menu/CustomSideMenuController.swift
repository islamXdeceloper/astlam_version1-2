//
//  CustomSideMenuController.swift
//  Astlam
//
//  Created by Eslam Ahmed on 7/1/20.
//  Copyright © 2020 Eslam Ahmed. All rights reserved.
//


import Foundation
import SideMenuController

class CustomSideMenuController: SideMenuController {
    
    override func viewDidLoad() {
        super.viewDidLoad()

        performSegue(withIdentifier: "showCenterController1", sender: nil)
        performSegue(withIdentifier: "containSideMenu", sender: nil)
        view.backgroundColor = .black
        

    }

    

  
    required init?(coder aDecoder: NSCoder) {
        SideMenuController.preferences.drawing.menuButtonImage = UIImage(named: "menu")
        SideMenuController.preferences.drawing.sidePanelPosition = .overCenterPanelRight
        if UIDevice.current.userInterfaceIdiom == .pad {
            SideMenuController.preferences.drawing.sidePanelWidth = 450

        } else if UIDevice.current.userInterfaceIdiom == .phone {
             SideMenuController.preferences.drawing.sidePanelWidth = 275
        }

        SideMenuController.preferences.drawing.centerPanelShadow = true
        SideMenuController.preferences.animating.statusBarBehaviour = .showUnderlay
        super.init(coder: aDecoder)
    }
    
}
