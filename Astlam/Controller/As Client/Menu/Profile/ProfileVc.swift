//
//  ProfileVc.swift
//  Astlam
//
//  Created by Eslam Ahmed on 6/13/20.
//  Copyright © 2020 Eslam Ahmed. All rights reserved.
//

import UIKit
import Kingfisher



class ProfileVc: UIViewController {

    
    
    @IBOutlet weak var ProfileImage: UIImageView!
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var phonenumber: UITextField!
    @IBOutlet weak var mandopLocation: UITextField!
    
    
    
    
//    var profile_data = ClientAuth_Getprofile_Struct()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        ProfileImage.layer.cornerRadius = view.frame.width / 2
        
        GetUserProfile()
    }
    
    
    
    
    
 fileprivate   func GetUserProfile(){
        
    LoadingIndicator.shared.Show(indicator: self.view)
    
    APIs.ClientAuth_GetProfile { (error, profile) in
        if let profile = profile {
            
            LoadingIndicator.shared.Hide()
            
            UIView.animate(withDuration: 0.5) {
//                self.view.isHidden = false
                self.username.text = profile.fullName ?? ""
                self.phonenumber.text = profile.phone ?? ""
                self.mandopLocation.text = profile.district ?? ""
                
                if let image = profile.image {

                    
                    self.ProfileImage.kf.setImage(with: URL(string: URLs.base_image_url + image))
                    
                    

                }
                
                self.view.layoutIfNeeded()
            }
            
            
        }else {
           
//            self.view.isHidden = true
            

            LoadingIndicator.shared.Hide()
            
            Loader.ShowError("حدث خطا ما")

        }
    }
    
    
    
    }
    
    
    
    
    
    @IBAction func changeImage(_ sender: Any) {
    }
    
    
    
    
    
    
    @IBAction func dismissTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    
    

    @IBAction func saveTapped(_ sender: Any) {
    }
    
    
    
    
    
    @IBAction func usernameEdit(_ sender: Any) {
        username.isUserInteractionEnabled = true
    }
    
    
    @IBAction func loactionEdit(_ sender: Any) {
        mandopLocation.isUserInteractionEnabled = true
    }
    @IBAction func phoneEdit(_ sender: Any) {
        phonenumber.isUserInteractionEnabled = true
    }
}
