//
//  OrderPriceCell.swift
//  Astlam
//
//  Created by Eslam Ahmed on 6/7/20.
//  Copyright © 2020 Eslam Ahmed. All rights reserved.
//

import UIKit

class OrderPriceCell: UITableViewCell {

    @IBOutlet weak var imageProfile: UIImageViewX!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var userDetails: UILabel!
    
    @IBOutlet weak var rating: UIImageView!
    
    @IBOutlet weak var price: UIButtonX!
    
    @IBOutlet weak var accept: UIButtonX!
    
    
    
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
