//
//  OrderPriceVC.swift
//  Astlam
//
//  Created by Eslam Ahmed on 6/7/20.
//  Copyright © 2020 Eslam Ahmed. All rights reserved.
//

import UIKit
import SideMenuController

class OrderPriceVC: UIViewController,SideMenuControllerDelegate {

    
    func sideMenuControllerDidHide(_ sideMenuController: SideMenuController) {
          print(#function)
      }
      
      func sideMenuControllerDidReveal(_ sideMenuController: SideMenuController) {
          print(#function)
      }
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var cancleBtn: UIButtonX!
    
    
    
    var number = ["hi","hello","why","done"]
    var customBar: UINavigationBar = UINavigationBar()

    override func viewDidLoad() {
        super.viewDidLoad()

       self.customBar.frame = CGRect(x:0, y:0, width:view.frame.width, height:(navigationController?.navigationBar.frame.height)! + 50)
        
     
        sideMenuController?.delegate = self

        // Do any additional setup after loading the view.
    }
  
    
    @IBAction func cancleTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
   
    
    @IBAction func backBtn(_ sender: Any) {
         self.dismiss(animated: true, completion: nil)
    }
    
    
    
    @IBAction func acceptBtn(_ sender: Any) {
        
             let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "OrderAcceptVC")
                         vc.modalPresentationStyle = .fullScreen
                         vc.modalTransitionStyle = .crossDissolve
                         show(vc, sender: self)
        
    }
    
}



extension OrderPriceVC: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return number.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "OrderPriceCell", for: indexPath) as! OrderPriceCell
        
        cell.username.text = number[indexPath.row]
        return cell
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
}
