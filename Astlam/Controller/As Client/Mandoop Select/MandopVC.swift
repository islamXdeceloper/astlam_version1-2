//
//  MandopVC.swift
//  Astlam
//
//  Created by Eslam Ahmed on 6/7/20.
//  Copyright © 2020 Eslam Ahmed. All rights reserved.
//

//import UIKit
//
//class MandopVC: UIViewController {
//
//    @IBOutlet weak var dropGo: UIImageView!
//    @IBOutlet weak var dropDown: UIImageView!
//    @IBOutlet weak var cityBtn: UIButton!
//
//
//
//    //MARK:- Variable & Constants
//
//    var id_Government = 0
//
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        // Do any additional setup after loading the view.
//        NotificationCenter.default.addObserver(self, selector: #selector(HandelCityName), name: .SaveCity, object: nil)
//
//    }
//
//    @objc func HandelCityName(notification: Notification) {
//           let cityVc = notification.object as! CityViewController
//         cityBtn.setTitle(cityVc.selectName, for: .normal)
//           id_Government = cityVc.selectNumber
//        dropGo.isHidden = false
//        dropDown.isHidden = true
//
//
//       }
//
//    @IBAction func cityTapped(_ sender: Any) {
//
//        dropGo.isHidden = true
//        let storyboard = UIStoryboard(name: "CustomAlert", bundle: nil)
//         let vc = storyboard.instantiateViewController(withIdentifier: "CityViewController")
//
//         self.present(vc, animated: true, completion: nil)
//
//
//
//    }
//
//    @IBAction func Continue(_ sender: Any) {
//        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "CurrentLocationVC")
//                  vc.modalPresentationStyle = .fullScreen
//                  vc.modalTransitionStyle = .crossDissolve
//                  show(vc, sender: self)
//    }
//
//}




import UIKit

class MandopVC: UIViewController {

    @IBOutlet weak var dropGo: UIImageView!
    @IBOutlet weak var dropDown: UIImageView!
    @IBOutlet weak var cityBtn: UIButton!
    
    var id_Government = 0
    var  Order : Order_GetDistrict!
    weak var observer : NSObjectProtocol?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//     observer =   NotificationCenter.default.addObserver(self, selector: #selector(HandelCityName), name: .SaveCity, object: nil)
        
        
    observer = NotificationCenter.default.addObserver(forName: .SaveCity, object: nil, queue: nil) { (notification) in
                       let cityVc = notification.object as! CityViewController
                    
                    self.Order = cityVc.selectObject
                    
                    
            self.cityBtn.setTitle(self.Order.districtName, for: .normal)
            //           id_Government = cityVc.selectNumber
                    
            //        print("id_Government = \(id_Government)")
            self.dropGo.isHidden = false
            self.dropDown.isHidden = true


        }
                    
    }
    
    
//    @objc func HandelCityName(notification: Notification) {
//
//           let cityVc = notification.object as! CityViewController
//
//        self.Order = cityVc.selectObject
//
//
//        cityBtn.setTitle(self.Order.districtName, for: .normal)
////           id_Government = cityVc.selectNumber
//
////        print("id_Government = \(id_Government)")
//        dropGo.isHidden = false
//        dropDown.isHidden = true
//
//
//       }
    
    @IBAction func cityTapped(_ sender: Any) {
        
        
        dropGo.isHidden = true
        let storyboard = UIStoryboard(name: "CustomAlert", bundle: nil)
         let vc = storyboard.instantiateViewController(withIdentifier: "CityViewController")
         
         self.present(vc, animated: true, completion: nil)
       
        
        
    }
    
    @IBAction func Continue(_ sender: Any) {
        
        if Order.id! > 0 {

            
        let sb = UIStoryboard(name: "Main", bundle: nil)
            
            let vc = sb.instantiateViewController(identifier: "CustomSideMenuController") as CustomSideMenuController
                  vc.modalPresentationStyle = .fullScreen
                  vc.modalTransitionStyle = .crossDissolve
            
            
                  show(vc, sender: self)
            }
                   else {
                       Loader.ShowError("يجب اختيار المنطقه ")
                   }
    }
    
    
    
    
    
    // to remove notification center when the page is disappear
    override func viewDidDisappear(_ animated: Bool) {
        if let observer = observer {
            NotificationCenter.default.removeObserver(observer)
        }
    }
    
}
