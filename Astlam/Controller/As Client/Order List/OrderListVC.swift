//
//  OrderListVC.swift
//  Astlam
//
//  Created by Eslam Ahmed on 6/8/20.
//  Copyright © 2020 Eslam Ahmed. All rights reserved.
//

import UIKit
import SideMenuController

class OrderListVC: UIViewController,SideMenuControllerDelegate {
      func sideMenuControllerDidHide(_ sideMenuController: SideMenuController) {
         print(#function)
     }
     
     func sideMenuControllerDidReveal(_ sideMenuController: SideMenuController) {
         print(#function)
     }
    
 
      
    
   
    @IBOutlet weak var descriptionOrder: UITextView!
    
    @IBOutlet weak var descriptionLocation: UITextView!
    
    @IBOutlet weak var sendToAll: UIButtonX!
    
    @IBOutlet weak var cancleBtn: UIButtonX!
    
    var window: UIWindow?

    override func viewDidLoad() {
        super.viewDidLoad()
//        addSlideMenuButton()
  sideMenuController?.delegate = self

        // Do any additional setup after loading the view.
        let borderGray = UIColor(red: 0.8, green: 0.8, blue: 0.8, alpha: 1)
           self.descriptionOrder.layer.borderColor = borderGray.cgColor
           self.descriptionOrder.layer.borderWidth = 1
           self.descriptionOrder.layer.cornerRadius = 2
        
        self.descriptionLocation.layer.borderColor = borderGray.cgColor
        self.descriptionLocation.layer.borderWidth = 1
        self.descriptionLocation.layer.cornerRadius = 2
        
        
    }
    
    @IBAction func sendTapped(_ sender: Any) {

        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "CustomThree")
               vc.modalPresentationStyle = .fullScreen
               vc.modalTransitionStyle = .crossDissolve
        present(vc, animated: true, completion: nil)
        
    }
    
    
    @IBAction func CancleTapped(_ sender: Any) {
        dismiss(animated: true)
    }
    
    @IBAction func backTapped(_ sender: Any) {
        dismiss(animated: true)

    }
    

}
