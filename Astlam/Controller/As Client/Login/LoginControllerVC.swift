//  ViewController.swift
//  Astlam
//  Created by Eslam Ahmed on 6/4/20.
//  Copyright © 2020 Eslam Ahmed. All rights reserved.


import UIKit
import StatusAlert

class LoginControllerVC: UIViewController {

    @IBOutlet weak var phoneNumber: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
   
        }

    @IBAction func dismissTapped(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    
    @IBAction func tappedVerify(_ sender: Any) {
        view.endEditing(true)
        guard let phone = phoneNumber.text, !phone.isEmpty else {
            let statusAlert = StatusAlert()
             statusAlert.image = UIImage(named: "error")
             statusAlert.title = "Phone Number Requierd"
             statusAlert.showInKeyWindow()
            return
        }
        
        
        LoadingIndicator.shared.Show(indicator: self.view)
        APIs.ClientAuth_CheckPhone(phone) { (success) in
            if success {
                
                        LoadingIndicator.shared.Hide()

                let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "VerificationsVC") as! VerificationsVC
                   vc.modalPresentationStyle = .fullScreen
                   vc.modalTransitionStyle = .crossDissolve
                vc.phoneNumber = phone
                self.show(vc, sender: self)

            }else {
                
            }
        }
        

        
        

        
    }
    


}
