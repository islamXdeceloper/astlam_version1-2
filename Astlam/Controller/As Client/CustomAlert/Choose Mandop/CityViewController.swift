//
//  CityViewController.swift
//  Astlam
//
//  Created by Eslam Ahmed on 6/6/20.
//  Copyright © 2020 Eslam Ahmed. All rights reserved.
//

import UIKit

class CityViewController: UIViewController {

    
    
 
    //MARK:- <======= AutoLayout Constraints ========>
    
    @IBOutlet weak var tableView: UITableView!
    
    
    
    
    
    //MARK:- Variable & Constants : -
    // this variable to take object slected form GetDistrictArr Array
    var selectObject : Order_GetDistrict!
    
//    let locationName = ["مندوب العمرة","مندوب العزيزية","مندوب النزهة","مندوب الشوقية"]
    
    var GetDistrictArr = [Order_GetDistrict]()
    
    
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
//        NotificationCenter.default.post(name: .SaveCity, object: self)
            LoadGetDistrict()
        
        self.tableView.isHidden = true
        self.tableView.tableFooterView = UIView()

    }
    
    
    
    
    
    //MARK:- To Get All District API
    
    func LoadGetDistrict() {
        
        LoadingIndicator.shared.Show(indicator: self.view)
        
        APIs.GetDistrict { (error, get) in
            
            if let get = get {
                self.GetDistrictArr = get
                
                if self.GetDistrictArr.count > 0 {
                self.tableView.reloadData()
                    LoadingIndicator.shared.Hide()
                    self.tableView.isHidden = false


                }else {
                    print("error")
                    LoadingIndicator.shared.Hide()

                }

            }
        }
    }
    

}





//MARK:- Table View Delegate & Data Source

extension CityViewController : UITableViewDataSource , UITableViewDelegate{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return GetDistrictArr.count
    }
    
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CityCollectionViewCell", for: indexPath) as! CityCollectionViewCell
        
        cell.mandopLabel.text = GetDistrictArr[indexPath.row].districtName
        cell.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0)
        return cell

    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
         let districtName = GetDistrictArr[indexPath.row] as Order_GetDistrict
            
//        if let ditrictObject = districtName {
            self.selectObject = districtName

//        }
            
//            selectName = districtName
//            selectNumber = GetDistrictArr[indexPath.row].id!
        
        
                self.dismiss(animated: true, completion: nil)
                
                
                NotificationCenter.default.post(name: .SaveCity, object: self)

    }
    
    
  
    
 
    
}
