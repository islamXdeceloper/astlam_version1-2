//
//  File.swift
//  Astlam
//
//  Created by Mostafa  on 6/21/20.
//  Copyright © 2020 Eslam Ahmed. All rights reserved.
//

import Foundation


struct ClientAuth_Getprofile_Struct : Codable {
    
    
    
    var phone : String?
    var fullName : String?
    var image : String?
    var district : String?
    var city : String?
    var log : Double?
    var lat : Double?
    var token : String?
//    var clientAccounts : String?
//    var orderStates : String?
//    var connectionId : String?
    
    
    
    init() {
        self.phone = ""
        self.fullName = ""
        self.image = ""
        self.district = ""
        self.city = ""
        self.log = 0
        self.lat = 0
        self.token = ""
    }
}





/*
 
 {
     "phone": "0547899286",
     "fullName": "Waad",
     "image": "image",
     "district": "Makkah",
     "city": "Makkah",
     "log": 0.0,
     "lat": 0.0,
     "token": "fWt6QzuaR6O6nets3vhtV4:APA91bFDrC1C1gc5i5X8Rdt_7NB7dr3J2lZbNY0wQvfXcgOWPiOWXlA7iS41NICojzk3zkFCfdfDUBoy7wtqJgx8jC2M7OUcRtfU3fb0D1p9YtNHYL66flhl56-AtFgfy3AeUtC5flqC",
     "connectionId": null,
     "orders": null,
     "clientAccounts": null,
     "orderStates": null
 }
 
 
 */
