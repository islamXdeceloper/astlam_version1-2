//
//  GeneralApp_Struct.swift
//  Astlam
//
//  Created by Mostafa  on 6/22/20.
//  Copyright © 2020 Eslam Ahmed. All rights reserved.
//

import Foundation



struct  GeneralApp_Struct : Codable {
  
    var id : Int?
    var twitter : String?
    var instagram : String?
    var facebook : String?
    var version : String?
    var email : String?
    var phone : String?
    var termsAndPolicies : String?
    var about : String?
    
    init() {
        self.id = 0
        self.twitter = ""
        self.instagram = ""
        self.facebook = ""
        self.version = ""
        self.email = ""
        self.phone = ""
        self.termsAndPolicies = ""
        self.about = ""
    }
     
}




/*
 
 
 
 {
     "id": 1,
     "twitter": "https://mobile.twitter.com/estalm1",
     "instagram": "https://instagram.com/estalm2020",
     "facebook": "https://www.facebook.com/estalm.estalm",
     "version": "1.0",
     "email": "Estalem977@gmail.com",
     "phone": "0547899286",
     "termsAndPolicies": "",
     "about": "ستلم منصة لتوصيل الطلبات من اي مكان والى اي مكان عن طريق مناديب جاهزة لخدمتكم على مدار ٢٤ ساعة ،في\n\t ‏استلم نهدف لتقديم الراحة للعملاء عن طريق توصيل الطلبات لهم بكل حب وبشكل سلس وسريع ،في استلم يوجد لكل حي مناديب خاصة به لخدمة العملاء بشكل اسرع كما يتيح استلم للعملاء حرية اختيار السعر  من الخيارات المقدمة لهم من مزودي الخدمة بما يناسبهم،\n\t يمكن للعملاء استلم تتبع مزود الخدمة لهم عن طريق الخريطة ..تطبيق استلم مُصمم بطريقة سلسه وبسيطه وغير معقدة لتمكنكم من استخدامه بشكل اسرع..نتطلع لخدمتكم دائماً."
 }
 
 
 */
