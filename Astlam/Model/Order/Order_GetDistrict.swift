//
//  File.swift
//  Astlam
//
//  Created by Mostafa  on 6/21/20.
//  Copyright © 2020 Eslam Ahmed. All rights reserved.
//

import Foundation



struct  Order_GetDistrict : Codable {
    
    var id : Int?
    var districtName : String?
    var city : String?
    var lat : Double?
    var log : Double?
//    var providers : String?
    
    
}




/*
 
 
 [
     {
         "id": 4,
         "districtName": "مندوب الشرقية",
         "city": "مكة",
         "lat": 19.1290281,
         "log": 41.0834693,
         "providers": null
     },
     {
         "id": 3,
         "districtName": "مندوب العزيزية",
         "city": "مكة",
         "lat": 21.415878,
         "log": 39.8519244,
         "providers": null
     },
     {
         "id": 2,
         "districtName": "مندوب الشوقية",
         "city": "مكة",
         "lat": 21.3855802,
         "log": 39.7878053,
         "providers": null
     },
     {
         "id": 1,
         "districtName": "مندوب الهجرة",
         "city": "مكة",
         "lat": 21.372394,
         "log": 39.8491793,
         "providers": null
     }
 ]
 
 
 */
