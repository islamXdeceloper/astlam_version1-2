////
////  File.swift
////  eCommerce
////
////  Created by Mostafa on 11/28/19.
////  Copyright © 2019 pc. All rights reserved.
////
//
import Foundation
import StatusAlert


final class Loader {
    
    
    static func ShowError(_ message : String) {
       
        let statusAlert = StatusAlert()
        statusAlert.image = UIImage(named: "error")
        statusAlert.title = message

        statusAlert.showInKeyWindow()
        

    }
    
    
    static func ShowSuccess(_ message : String) {
        
        let statusAlert = StatusAlert()
        statusAlert.image = UIImage(named: "success")
        statusAlert.title = message

        statusAlert.showInKeyWindow()

    }
    
    
}

