//
//  CustomLoadingIndicator.swift
//  CusomizeActivityIndicator
//
//  Created by Mostafa  on 6/11/20.
//  Copyright © 2020 Mostafa . All rights reserved.
//
//import Foundation
//import UIKit
//import NVActivityIndicatorView
//
//
//class CustomLoadingIndicator: UIView {
//
//    public static let shared = CustomLoadingIndicator()
//
//
//    private var indicator : NVActivityIndicatorView =  {
//
//        let indicator = NVActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 60, height: 60), type: .ballClipRotatePulse, color: .black, padding: 2)
//
//
//
////        indicator.color = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
////        indicator.backgroundColor = UIColor.black.withAlphaComponent(0.4)
////        if #available(iOS 13.0, *) {
////            indicator.style = .large
////        } else {
////            // Fallback on earlier versions
////        }
//        indicator.translatesAutoresizingMaskIntoConstraints = false
//
//        return indicator
//    }()
//
//
//
//
//    func Show(indicator forView : UIView)  {
//
////        let windows = UIApplication.shared.keyWindow
//
////        let window = UIApplication.shared.connectedScenes
////        .filter({$0.activationState == .foregroundActive})
////        .map({$0 as? UIWindowScene})
////        .compactMap({$0})
////        .first?.windows
////        .filter({$0.isKeyWindow}).first
//
//
//        indicator.startAnimating()
//        forView.addSubview(indicator)
//
//
//
////        startAnimating( CGSize(width: 50, height: 50) , message: "loading ...", messageFont: nil, type: .ballClipRotatePulse , color: .black, padding: nil, displayTimeThreshold: nil, minimumDisplayTime: nil, backgroundColor: nil, textColor: .black, fadeInAnimation: nil)
//
//
////        indicator.frame = forView.frame
//
//
////        indicator.bottomAnchor.constraint(equalTo: forView.bottomAnchor, constant: 0).isActive = true
////        indicator.topAnchor.constraint(equalTo: forView.topAnchor, constant: 0).isActive = true
////        indicator.leftAnchor.constraint(equalTo: forView.leftAnchor, constant: 0).isActive = true
////        indicator.rightAnchor.constraint(equalTo: forView.rightAnchor, constant: 0).isActive = true
//
//
//        indicator.centerYAnchor.constraint(equalToSystemSpacingBelow: forView.centerYAnchor, multiplier: 0).isActive = true
//        indicator.centerXAnchor.constraint(equalToSystemSpacingAfter: forView.centerXAnchor, multiplier: 0).isActive = true
//
//        indicator.widthAnchor.constraint(equalToConstant: 50).isActive = true
//        indicator.heightAnchor.constraint(equalToConstant: 50).isActive = true
//
//        indicator.bringSubviewToFront(forView)
//    }
//
//
//
//    func Show(indcator forView : UIView , message : String) {
//
//    }
//
//    
//    func Hide (){
//        indicator.stopAnimating()
////        indicator.hidesWhenStopped
//    }
//}
//
//
//
//
//extension CustomLoadingIndicator : NVActivityIndicatorViewable{
//
//}
