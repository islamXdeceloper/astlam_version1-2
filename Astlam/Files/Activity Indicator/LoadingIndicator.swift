//
//  ActivityIndicator.swift
//  CusomizeActivityIndicator
//
//  Created by Mostafa  on 6/11/20.
//  Copyright © 2020 Mostafa . All rights reserved.
//

import UIKit


class LoadingIndicator: UIView {
    
    public static let shared = LoadingIndicator()
    
    
    private var indicator : UIActivityIndicatorView =  {
        
        let indicator = UIActivityIndicatorView()
        indicator.color = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        indicator.backgroundColor = UIColor.black.withAlphaComponent(0.4)
        if #available(iOS 13.0, *) {
            indicator.style = .large
        } else {
            // Fallback on earlier versions
        }
        indicator.translatesAutoresizingMaskIntoConstraints = false
        
        return indicator
    }()
    
    
    
    
    func Show(indicator forView : UIView)  {
        
//        let windows = UIApplication.shared.keyWindow
        
//        let window = UIApplication.shared.connectedScenes
//        .filter({$0.activationState == .foregroundActive})
//        .map({$0 as? UIWindowScene})
//        .compactMap({$0})
//        .first?.windows
//        .filter({$0.isKeyWindow}).first

        
        indicator.startAnimating()
        forView.addSubview(indicator)
        
        indicator.frame = forView.frame
        indicator.bottomAnchor.constraint(equalTo: forView.bottomAnchor, constant: 0).isActive = true

        indicator.topAnchor.constraint(equalTo: forView.topAnchor, constant: 0).isActive = true
        indicator.leftAnchor.constraint(equalTo: forView.leftAnchor, constant: 0).isActive = true
        indicator.rightAnchor.constraint(equalTo: forView.rightAnchor, constant: 0).isActive = true
        
        indicator.bringSubviewToFront(forView)
    }
    
    
    
    func Hide (){
        indicator.stopAnimating()
//        indicator.hidesWhenStopped
    }
}


