////
////  AppDelegate.swift
////  Astlam
////
////  Created by Eslam Ahmed on 6/4/20.
////  Copyright © 2020 Eslam Ahmed. All rights reserved.
////
//


import UIKit
import UserNotifications
import Firebase
import FirebaseMessaging
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {


    var window: UIWindow?
    let gcmMessageIDKey = "gcm.message_id"
    

    
    //this func go to the main Home View Controller
           
    func restartApp() {
        
        if helper.get_Phone_Number() != nil {
              let sb = UIStoryboard(name: "Main", bundle: nil)
        
        // if user login as client
                if helper.get_type() == 1 {
                    
                    let vc = sb.instantiateViewController(withIdentifier: "CustomSideMenuController")
                           window?.rootViewController = vc
                }
                    
                // if user login as providers
                else if helper.get_type() ==  2{
                   
                    let vc = sb.instantiateViewController(withIdentifier: "CustomSideMenuController")
                    window?.rootViewController = vc

                }
        }

    }
    
    
    

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.

        
        
               
        IQKeyboardManager.shared.enable = true
             IQKeyboardManager.shared.enableDebugging = true
             IQKeyboardManager.shared.overrideKeyboardAppearance = true
             IQKeyboardManager.shared.keyboardAppearance = .dark
             IQKeyboardManager.shared.keyboardDistanceFromTextField = 100.0
             IQKeyboardManager.shared.toolbarTintColor = .gray
             IQKeyboardManager.shared.toolbarTintColor = .gray
             IQKeyboardManager.shared.shouldShowToolbarPlaceholder = true
             IQKeyboardManager.shared.shouldResignOnTouchOutside = true
             IQKeyboardManager.shared.shouldPlayInputClicks = true
        
        print("type ===== \(helper.get_type())")
        print("phone number ========> \(helper.get_Phone_Number()!) ")
        restartApp()
        
        FirebaseApp.configure()

        if #available(iOS 10.0, *) {
            UNUserNotificationCenter.current().delegate = self
            Messaging.messaging().delegate = self


            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
        let token = Messaging.messaging().fcmToken
        print("FCM token: \(token ?? "")")
        helper.saveFirebaseToken(FCMtoken: token ?? "")
        let center = UNUserNotificationCenter.current()
        center.requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in

            // If granted comes true you can enabled features based on authorization.
            guard granted else { return }
            DispatchQueue.main.async {
                application.registerForRemoteNotifications()
            }
        }
        
        return true
    }

    // MARK: UISceneSession Lifecycle


    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
          // If you are receiving a notification message while your app is in the background,
          // this callback will not be fired till the user taps on the notification launching the application.
          // TODO: Handle data of notification

          // With swizzling disabled you must let Messaging know about the message, for Analytics
          // Messaging.messaging().appDidReceiveMessage(userInfo)

          // Print message ID.
          if let messageID = userInfo[gcmMessageIDKey] {
              print("Message ID: \(messageID)")
          }

          // Print full message.
          print(userInfo)
      }

      func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                       fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {

          if let messageID = userInfo[gcmMessageIDKey] {
              print("Message ID: \(messageID)")
          }

          // Print full message.
          print(userInfo)

          completionHandler(UIBackgroundFetchResult.newData)
      }


}



extension AppDelegate: MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        let dataDict:[String: String] = ["token": fcmToken]
        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)


        print(fcmToken)

    }
     func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
        print("message:",remoteMessage.appData)
    }

}
@available(iOS 10, *)
extension AppDelegate : UNUserNotificationCenterDelegate {

    // Receive displayed notifications for iOS 10 devices.
    func userNotificationCenter(_ center: UNUserNotificationCenter,willPresent notification: UNNotification,withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        let userInfo = notification.request.content.userInfo
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        print(userInfo)

        completionHandler([.alert,.sound,.badge])
    }

    func userNotificationCenter(_ center: UNUserNotificationCenter,didReceive response: UNNotificationResponse,withCompletionHandler completionHandler: @escaping () -> Void) {
        let userInfo = response.notification.request.content.userInfo
        // Print message ID.
        if let messageID = userInfo[gcmMessageIDKey] {
            print("Message ID: \(messageID)")
        }
        // Print full message.
        print(userInfo)

        completionHandler()
    }

}



/////import UIKit
//import UserNotifications
//import Firebase
//import FirebaseMessaging
//import SideMenuController
//
//@UIApplicationMain
//class AppDelegate: UIResponder, UIApplicationDelegate {
//
//
//    var window: UIWindow?
//    let gcmMessageIDKey = "gcm.message_id"
//
//
//    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
//        // Override point for customization after application launch.
//
//        SideMenuController.preferences.drawing.menuButtonImage = UIImage(named: "menu")
//          SideMenuController.preferences.drawing.sidePanelPosition = .overCenterPanelRight
//          if UIDevice.current.userInterfaceIdiom == .pad {
//              SideMenuController.preferences.drawing.sidePanelWidth = 450
//
//          } else if UIDevice.current.userInterfaceIdiom == .phone {
//              SideMenuController.preferences.drawing.sidePanelWidth = 275
//          }
//
//          SideMenuController.preferences.drawing.centerPanelShadow = true
//          SideMenuController.preferences.animating.statusBarBehaviour = .showUnderlay
//
//
//        FirebaseApp.configure()
//
//        if #available(iOS 10.0, *) {
//            UNUserNotificationCenter.current().delegate = self
//            Messaging.messaging().delegate = self
//
//
//            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
//            UNUserNotificationCenter.current().requestAuthorization(
//                options: authOptions,
//                completionHandler: {_, _ in })
//        } else {
//            let settings: UIUserNotificationSettings =
//                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
//            application.registerUserNotificationSettings(settings)
//        }
//
//        application.registerForRemoteNotifications()
//        let token = Messaging.messaging().fcmToken
//        print("FCM token: \(token ?? "")")
//        helper.saveFirebaseToken(FCMtoken: token ?? "")
//        let center = UNUserNotificationCenter.current()
//        center.requestAuthorization(options:[.badge, .alert, .sound]) { (granted, error) in
//
//            // If granted comes true you can enabled features based on authorization.
//            guard granted else { return }
//            DispatchQueue.main.async {
//                application.registerForRemoteNotifications()
//            }
//        }
//
//
//        return true
//    }
//
//    // MARK: UISceneSession Lifecycle
//
//    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
//        // Called when a new scene session is being created.
//        // Use this method to select a configuration to create the new scene with.
//        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
//    }
//
//    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
//        // Called when the user discards a scene session.
//        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
//        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
//    }
//    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
//          // If you are receiving a notification message while your app is in the background,
//          // this callback will not be fired till the user taps on the notification launching the application.
//          // TODO: Handle data of notification
//
//          // With swizzling disabled you must let Messaging know about the message, for Analytics
//          // Messaging.messaging().appDidReceiveMessage(userInfo)
//
//          // Print message ID.
//          if let messageID = userInfo[gcmMessageIDKey] {
//              print("Message ID: \(messageID)")
//          }
//
//          // Print full message.
//          print(userInfo)
//      }
//
//      func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
//                       fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
//
//          if let messageID = userInfo[gcmMessageIDKey] {
//              print("Message ID: \(messageID)")
//          }
//
//          // Print full message.
//          print(userInfo)
//
//          completionHandler(UIBackgroundFetchResult.newData)
//      }
//
//
//}
//
//
//
//extension AppDelegate: MessagingDelegate {
//    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
//        let dataDict:[String: String] = ["token": fcmToken]
//        NotificationCenter.default.post(name: Notification.Name("FCMToken"), object: nil, userInfo: dataDict)
//
//
//        print(fcmToken)
//
//    }
//     func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
//        print("message:",remoteMessage.appData)
//    }
//
//}
//@available(iOS 10, *)
//extension AppDelegate : UNUserNotificationCenterDelegate {
//
//    // Receive displayed notifications for iOS 10 devices.
//    func userNotificationCenter(_ center: UNUserNotificationCenter,willPresent notification: UNNotification,withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
//        let userInfo = notification.request.content.userInfo
//        if let messageID = userInfo[gcmMessageIDKey] {
//            print("Message ID: \(messageID)")
//        }
//        print(userInfo)
//
//        completionHandler([.alert,.sound,.badge])
//    }
//
//    func userNotificationCenter(_ center: UNUserNotificationCenter,didReceive response: UNNotificationResponse,withCompletionHandler completionHandler: @escaping () -> Void) {
//        let userInfo = response.notification.request.content.userInfo
//        // Print message ID.
//        if let messageID = userInfo[gcmMessageIDKey] {
//            print("Message ID: \(messageID)")
//        }
//        // Print full message.
//        print(userInfo)
//
//        completionHandler()
//    }
//
//}
