


//
//  Helper.swift
//  TURBO
//
//  Created by a on 1/23/20.
//  Copyright © 2020 pc. All rights reserved.
//

import UIKit

class helper : NSObject {
    
    
    
    //MARK:- This save Type between (Client & Provider)
    /// if  save 1    ==========>    Client
    /// if save 2     ==========>    Providers
    
    static func save_Type(_ type : Int) {
        
        UserDefaults.standard.set(type , forKey: "type")
        UserDefaults.standard.synchronize()

    }
    
    
    //MARK :- To Get Type
    
    static func get_type() -> Int {
        return  UserDefaults.standard.integer(forKey: "type")
    }

    
    
    static func remove_type(){
        
                let defaults = UserDefaults.standard
                defaults.removeObject(forKey: "type")

    }
    
    
    
//
    static func remove_all(){

        let defaults = UserDefaults.standard
            let dictionary = defaults.dictionaryRepresentation()
            dictionary.keys.forEach { key in
                defaults.removeObject(forKey: key)
            }
        }
    
    
    
    //MARK:- this func to Save Phone Number in :-
    
    static func save_Phone_Number(_ phone_number : String){
        UserDefaults.standard.set(phone_number , forKey: "Phone_Number")
        UserDefaults.standard.synchronize()
        
    }
    
    
    //To Get Phone number from UserDefaults : -
    
    static func get_Phone_Number() -> String? {
        
        return  UserDefaults.standard.string(forKey: "Phone_Number") ?? ""
    }

    
    
    
    
    
    
    //MARK:- this func to save the api_token to UserDefaults
    
//    class func saveToken(token : String){
//        // Save api token to UserDefaults
//        let def = UserDefaults.standard
//        def.set(token, forKey: "api_token")
//        def.synchronize()
//        //  restartApp()
//    }
//    class func getApiToken() -> String? {
//        let def = UserDefaults.standard
//        return def.object(forKey: "api_token") as? String
//    }
//
//
//
//    // To Remove Api_Token From User Defaults
//    class func Remove_Api_Token(){
//        let defaults = UserDefaults.standard
//        defaults.removeObject(forKey: "api_token")
//
//    }
//
//
//
//
//
//
//
    // Save Token Firebase
    class func saveFirebaseToken(FCMtoken : String){
        // Save api token to UserDefaults
        let def = UserDefaults.standard
        def.set(FCMtoken, forKey: "firebase_token")
        def.synchronize()
    }
    //get the firebase token

    class func getApiFirebaseToken() -> String? {
        let def = UserDefaults.standard
        return def.object(forKey: "firebase_token") as? String
    }
    //remove firebase token when logout
    class func Remove_Api_Firebase_Token(){
        let defaults = UserDefaults.standard
        defaults.removeObject(forKey: "firebase_token")

    }
//
//
//
//
//    //MARK:- this func to Save Name_User in UserDefaults:-
//
//    class func save_User_Name(_ user_Name : String){
//        UserDefaults.standard.set(user_Name , forKey: "User_Name")
//        UserDefaults.standard.synchronize()
//
//    }
//
//
//    //To Get max_order_updated_at from UserDefaults : -
//
//    class func get_max_order_updated_at() -> String {
//
//        return  UserDefaults.standard.string(forKey: "max_order") ?? ""
//    }
//
//
//    //MARK:- this func to Save max_order_updated_at in UserDefaults:-
//
//    class func save_max_order_updated_at(_ max_order : String){
//        UserDefaults.standard.set(max_order , forKey: "max_order")
//        UserDefaults.standard.synchronize()
//
//    }
//
//    // To Remove Api_Token From User Defaults
//    class func Remove_max_order(){
//        let defaults = UserDefaults.standard
//        defaults.removeObject(forKey: "max_order")
//
//
//    }
//
//
//    //To Get max_order_updated_at from UserDefaults : -
//
//    class func get_max_notification_id() -> String {
//
//        return  UserDefaults.standard.string(forKey: "max_notification_id") ?? ""
//    }
//
//
//    //MARK:- this func to Save max_order_updated_at in UserDefaults:-
//
//    class func save_max_notification_id(_ max_order : String){
//        UserDefaults.standard.set(max_order , forKey: "max_notification_id")
//        UserDefaults.standard.synchronize()
//
//    }
//
//    // To Remove Api_Token From User Defaults
//    class func Remove_max_notification_id(){
//        let defaults = UserDefaults.standard
//        defaults.removeObject(forKey: "max_notification_id")
//
//
//    }
//
//    class func save_label_notifications(_ save_number: String) {
//        UserDefaults.standard.set(save_number , forKey: "badge")
//        UserDefaults.standard.synchronize()
//    }
//
//    class func remove_label_notifications() {
//        let defaults = UserDefaults.standard
//        defaults.removeObject(forKey: "badge")
//
//    }
//
//
//
//    //To Get user Name from UserDefaults : -
//
//    class func get_User_Name() -> String {
//
//        return  UserDefaults.standard.string(forKey: "User_Name") ?? ""
//    }
//
//
//
//
//
//
//
//
//
//
//  //  MARK:- this func to Save Email_User in UserDefaults:-
//
//    class func save_User_Email(_ user_Email : String){
//        UserDefaults.standard.set(user_Email , forKey: "User_Email")
//        UserDefaults.standard.synchronize()
//
//    }
//
//
//    //To Get user Email from UserDefaults : -
//
//    class func get_User_Email() -> String {
//
//        return  UserDefaults.standard.string(forKey: "User_Email")!
//    }
//
//
//
//    //MARK:- this func to save the Catogery_Id in UserDefaults
//
//    class func save_User_Id(_ user_Id : Int){
//        UserDefaults.standard.set(user_Id , forKey: "User_Id")
//        UserDefaults.standard.synchronize()
//
//    }
//
//
//    //MARK:- To get the Catogery_Id From UserDefaults:-
//
//    class func get_User_Id()-> Int{
//        return UserDefaults.standard.integer(forKey: "User_Id")
//    }
//
//
//    //MARK:- To Save Code_Token_Forget pasword
//    class func save_Code(code : String){
//        // Save Code to UserDefaults
//        let def = UserDefaults.standard
//        def.set(code, forKey: "Api_Code")
//        def.synchronize()
//    }
//
//    //MARK:- To get code from api
//
//    class func get_Code() -> String {
//
//        return UserDefaults.standard.string(forKey: "Api_Code")!
//    }
    
}

////  Helper.swift
////  TURBO
////
////  Created by a on 1/23/20.
////  Copyright © 2020 pc. All rights reserved.
////
//
//import UIKit
//
//class helper : NSObject {
//
//
//
//    //MARK:- This save Type between (Client & Provider)
//    /// if  save 1    ==========>    Client
//    /// if save 2     ==========>    Providers
//
//    static func save_Type(_ type : Int) {
//
//        UserDefaults.standard.set(type , forKey: "type")
//        UserDefaults.standard.synchronize()
//
//    }
//
//
//    //MARK :- To Get Type
//
//    static func get_type() -> Int {
//        return  UserDefaults.standard.integer(forKey: "type")
//    }
//
//
//
//
//
//        //this func go to the main Home View Controller
//
//        static func restartApp(){
//
//            //MARK:- to Choose client
//            if helper.get_type() == 1 {
//
//                        guard let window = UIApplication.shared.keyWindow else {return}
//
//                        let sb = UIStoryboard(name: "Main", bundle: nil)
//                        let vc : UIViewController = sb.instantiateViewController(withIdentifier: "CustomSideMenuController")
//                        window.rootViewController = vc
//                        UIView.transition(with: window, duration: 0.5, options: .transitionFlipFromRight , animations: nil, completion: nil)
//
//            }
//            // to choose providers
//            else
//            {
//                        guard let window = UIApplication.shared.keyWindow else {return}
//
//                        let sb = UIStoryboard(name: "Main", bundle: nil)
//                        let vc : UIViewController = sb.instantiateViewController(withIdentifier: "CustomSideMenuController")
//                        window.rootViewController = vc
//                        UIView.transition(with: window, duration: 0.5, options: .transitionFlipFromRight , animations: nil, completion: nil)
//
//            }
//        }
//
//
//
//
//
//    //MARK:- this func to Save Phone Number in :-
//
//    static func save_Phone_Number(_ phone_number : String){
//        UserDefaults.standard.set(phone_number , forKey: "Phone_Number")
//        UserDefaults.standard.synchronize()
//
//    }
//
//
//    //To Get Phone number from UserDefaults : -
//
//    static func get_Phone_Number() -> String {
//
//        return  UserDefaults.standard.string(forKey: "Phone_Number") ?? ""
//    }
//
//
//
//
//
//
//    //this func go to the main Home View Controller
//
////    class func restartApp(){
////        guard let window = UIApplication.shared.keyWindow else {return}
////
////        let sb = UIStoryboard(name: "Main", bundle: nil)
////        let vc : UIViewController = sb.instantiateViewController(withIdentifier: "CustomSideMenuController")
////        window.rootViewController = vc
////        UIView.transition(with: window, duration: 0.5, options: .transitionFlipFromRight , animations: nil, completion: nil)
////    }
//
//    //MARK:- this func to save the api_token to UserDefaults
//
////    class func saveToken(token : String){
////        // Save api token to UserDefaults
////        let def = UserDefaults.standard
////        def.set(token, forKey: "api_token")
////        def.synchronize()
////        //  restartApp()
////    }
////    class func getApiToken() -> String? {
////        let def = UserDefaults.standard
////        return def.object(forKey: "api_token") as? String
////    }
////
////
////
////    // To Remove Api_Token From User Defaults
////    class func Remove_Api_Token(){
////        let defaults = UserDefaults.standard
////        defaults.removeObject(forKey: "api_token")
////
////    }
////
////
////
////
////
////
////
//    // Save Token Firebase
//    class func saveFirebaseToken(FCMtoken : String){
//        // Save api token to UserDefaults
//        let def = UserDefaults.standard
//        def.set(FCMtoken, forKey: "firebase_token")
//        def.synchronize()
//    }
//    //get the firebase token
//
//    class func getApiFirebaseToken() -> String? {
//        let def = UserDefaults.standard
//        return def.object(forKey: "firebase_token") as? String
//    }
//    //remove firebase token when logout
//    class func Remove_Api_Firebase_Token(){
//        let defaults = UserDefaults.standard
//        defaults.removeObject(forKey: "firebase_token")
//
//    }
////
////
////
////
////    //MARK:- this func to Save Name_User in UserDefaults:-
////
////    class func save_User_Name(_ user_Name : String){
////        UserDefaults.standard.set(user_Name , forKey: "User_Name")
////        UserDefaults.standard.synchronize()
////
////    }
////
////
////    //To Get max_order_updated_at from UserDefaults : -
////
////    class func get_max_order_updated_at() -> String {
////
////        return  UserDefaults.standard.string(forKey: "max_order") ?? ""
////    }
////
////
////    //MARK:- this func to Save max_order_updated_at in UserDefaults:-
////
////    class func save_max_order_updated_at(_ max_order : String){
////        UserDefaults.standard.set(max_order , forKey: "max_order")
////        UserDefaults.standard.synchronize()
////
////    }
////
////    // To Remove Api_Token From User Defaults
////    class func Remove_max_order(){
////        let defaults = UserDefaults.standard
////        defaults.removeObject(forKey: "max_order")
////
////
////    }
////
////
////    //To Get max_order_updated_at from UserDefaults : -
////
////    class func get_max_notification_id() -> String {
////
////        return  UserDefaults.standard.string(forKey: "max_notification_id") ?? ""
////    }
////
////
////    //MARK:- this func to Save max_order_updated_at in UserDefaults:-
////
////    class func save_max_notification_id(_ max_order : String){
////        UserDefaults.standard.set(max_order , forKey: "max_notification_id")
////        UserDefaults.standard.synchronize()
////
////    }
////
////    // To Remove Api_Token From User Defaults
////    class func Remove_max_notification_id(){
////        let defaults = UserDefaults.standard
////        defaults.removeObject(forKey: "max_notification_id")
////
////
////    }
////
////    class func save_label_notifications(_ save_number: String) {
////        UserDefaults.standard.set(save_number , forKey: "badge")
////        UserDefaults.standard.synchronize()
////    }
////
////    class func remove_label_notifications() {
////        let defaults = UserDefaults.standard
////        defaults.removeObject(forKey: "badge")
////
////    }
////
////
////
////    //To Get user Name from UserDefaults : -
////
////    class func get_User_Name() -> String {
////
////        return  UserDefaults.standard.string(forKey: "User_Name") ?? ""
////    }
////
////
////
////
////
////
////
////
////
////
////  //  MARK:- this func to Save Email_User in UserDefaults:-
////
////    class func save_User_Email(_ user_Email : String){
////        UserDefaults.standard.set(user_Email , forKey: "User_Email")
////        UserDefaults.standard.synchronize()
////
////    }
////
////
////    //To Get user Email from UserDefaults : -
////
////    class func get_User_Email() -> String {
////
////        return  UserDefaults.standard.string(forKey: "User_Email")!
////    }
////
////
////
////    //MARK:- this func to save the Catogery_Id in UserDefaults
////
////    class func save_User_Id(_ user_Id : Int){
////        UserDefaults.standard.set(user_Id , forKey: "User_Id")
////        UserDefaults.standard.synchronize()
////
////    }
////
////
////    //MARK:- To get the Catogery_Id From UserDefaults:-
////
////    class func get_User_Id()-> Int{
////        return UserDefaults.standard.integer(forKey: "User_Id")
////    }
////
////
////    //MARK:- To Save Code_Token_Forget pasword
////    class func save_Code(code : String){
////        // Save Code to UserDefaults
////        let def = UserDefaults.standard
////        def.set(code, forKey: "Api_Code")
////        def.synchronize()
////    }
////
////    //MARK:- To get code from api
////
////    class func get_Code() -> String {
////
////        return UserDefaults.standard.string(forKey: "Api_Code")!
////    }
//
//}
